# -*- coding: utf-8 -*-
"""
    pyfittslaw.bin.circle
    ~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012,2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: Fitts' law click test
"""

import sys
import os
import time
import signal
import getopt
from PyQt4 import QtCore,Qt
from pyfittslaw.gui.widgets.fittslaw import FittsLawWidget
from pyfittslaw.simulator.driver import XdoDriver

signal.signal(signal.SIGINT,signal.SIG_DFL) # Ctrl+C stops application


def main(n_targets = None, target_height = None, target_width = None, 
        target_distance = None, sequence_mode = None, feedback = False,
        show_cursor = False):
    """
    run app and show circle widget

    [param n_targets] number of targets (default:13)
    [type n_targets] int

    [param target_height] height of target widgets [px] (default: 30)
    [type target_height] int

    [param target_width] width of target widgets [px] (default: 30)
    [type target_width] int

    [param target_distance] circle diameter [px]
    [type target_distance] int

    [param sequence_mode] target sequence order
    [type sequence_mode] FittsLawWidget.sequenceModi

    [param feedback] feedback on click. if true, user *must* 
        hit target and visual pointer feedback is shown
    [type feedback] boolean

    [param show_cursor] show visual cursor (default: True)
    [type shown_cursor] boolean
    """

    n_targets = n_targets or 13
    target_height = target_height or 30
    target_width = target_width or 30
    target_distance = target_distance or 320
    sequence_mode = sequence_mode or FittsLawWidget.sequenceModi[1]

    target_size=QtCore.QSize(target_height, target_width)

    app = Qt.QApplication(sys.argv)
    mw = FittsLawWidget(buttonCount = n_targets,
                            buttonSize = target_size,
                            buttonDistance = target_distance,
                            logHandler= sys.stdout,
                            sequenceMode = sequence_mode,
                            hide_inactive = False,
                            feedback_on_target_miss = feedback,
                            show_cursor = show_cursor)
    mw.show()
    return app.exec_()


def usage():
    """ print usage message """

    print("""usage: %s 
        -n, --n_targets    number_of_targets> (default:30)
        -d, --diameter     circle diameter (default:320)
        -p, --pointer      provide visual pointer feedback (true|false)
        -r, --require_hit  require target hit (true|false)
        """ % sys.argv[0])


if __name__ == "__main__":

    show_cursor = None
    feedback = False
    target_width, target_height = None, None
    sequence_mode = None
    n_targets = None
    target_distance = None

    # read command line parameter
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hn:d:p:r:",["n_targets=","diameter=","pointer=","require_hit="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-n", "--n_targets"):
            n_targets = int(arg)
        elif opt in ("-d", "--diameter"):
            target_distance = float(arg)
        elif opt in ("-p", "--pointer"):
            show_cursor = bool(str.lower(arg) == "true")
        elif opt in ("-r", "--require_hit"):
            feedback = bool(str.lower(arg) == "true")

    main(n_targets, target_height, target_width, target_distance, sequence_mode, feedback, show_cursor)

