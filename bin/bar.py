# -*- coding: utf-8 -*-
"""
    pyfittslaw.bin.bar
    ~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012,2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: 1D bar interaction test
"""

import sys
import os
import time
import signal
import getopt
from PyQt4 import QtCore, Qt
from pyfittslaw.gui.widgets.knightRiderTargets import KnightRiderTargets

signal.signal(signal.SIGINT,signal.SIG_DFL) # Ctrl+C stops application

if __name__ == "__main__":

    app = Qt.QApplication(sys.argv)

def main(n_targets = None, target_height = None, target_width = None, 
        target_distance = None, orientation = None, hide_inactive_targets = None, 
        feedback = False, show_cursor = False):
    """
    run app and show circle widget

    [param n_targets] number of targets
    [type n_targets] int

    [param target_height] height of target widgets [px]
    [type target_height] int

    [param target_width] width of target widgets [px]
    [type target_width] int

    [param target_distance] circle diameter [px]
    [type target_distance] int

    [param orientation] bar orientation 
    [type orientation] string in [horizontal, vertical]

    [param hide_inactive_targets] hide inactive targets
    [type hide_inactive_targets] boolean
    
    [param feedback] feedback on click. if true, user *must* 
        hit target and visual pointer feedback is shown
    [type feedback] boolean

    [param show_cursor] show visual cursor (default: True)
    [type shown_cursor] boolean
    """

    # parameter
    n_targets = n_targets or 3
    target_height = target_height or 30
    target_width = target_width or 30
    target_distance = target_distance or 320
    orientation = orientation or 'horizontal'

    target_size=QtCore.QSize(target_height, target_width)
    print hide_inactive_targets

    app = Qt.QApplication(sys.argv)
    mw = KnightRiderTargets(buttonCount = n_targets,
        buttonSize = target_size,
        buttonDistance = target_distance,
        logHandler = sys.stdout,
        hide_inactive = hide_inactive_targets,
        feedback_on_target_miss = feedback,
        show_cursor = show_cursor,
        orientation = orientation)

    mw.show()
    return app.exec_()

def usage():
    print("""usage: %s 
        -n, --n_targets                 number_of_targets> (default:3)
        -d, --distance                  target distance
        -p, --pointer                   provide visual pointer feedback (true|false)
        -r, --require_hit              require target hit (true|false)
        -o, --orientation               bar orientation (horizontal|vertical)
        -t, -- hide_inactive_targets    hide inactive targets (true|false)
        """ % sys.argv[0])


if __name__ == "__main__":

    show_cursor = True
    feedback = False
    hide_inactive_targets = False
    target_width, target_height = None, None
    n_targets = None
    target_distance = None
    orientation = None

    # read command line parameter
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hn:d:p:r:o:t:",
                ["n_targets=","distance=","pointer=","require_hit=",
                    "orientation=","hide_inactive_targets="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-n", "--n_targets"):
            n_targets = int(arg)
        elif opt in ("-d", "--distance"):
            target_distance = float(arg)
        elif opt in ("-p", "--pointer"):
            show_cursor = bool(str.lower(arg) == "true")
        elif opt in ("-r", "--require_hit"):
            feedback = bool(str.lower(arg) == "true")
        elif opt in ("-o", "--orientation"):
            orientation = str.lower(arg)
        elif opt in ("-t" "hide_inactive_targets="):
            hide_inactive_targets = bool(str.lower(arg) == "true")


    main(n_targets, target_height, target_width, target_distance, orientation, hide_inactive_targets, 
            feedback, show_cursor)

