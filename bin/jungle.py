# -*- coding: utf-8 -*-
"""
    pyfittslaw.bin.jungle
    ~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: jungle interaction app
"""


from PyQt4 import QtCore, QtGui

import sys
import signal
from pyfittslaw.gui.widgets.randoms import *

signal.signal(signal.SIGINT,signal.SIG_DFL) # Ctrl+C stops application

def main():
    background_image_filename = "./pyfittslaw/res/jungle.jpg"
    image_file_names = ["./pyfittslaw/res/apple.png", 
                        "./pyfittslaw/res/paul.png"]
    target_sizes = [[i*10,i*10] for i in range(4,10,2)]

    app = QtGui.QApplication(sys.argv)
    w = MovingTargets(background_image_filename = background_image_filename,
        image_file_names = image_file_names,
        target_sizes = target_sizes)
    w.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
