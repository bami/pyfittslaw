from PyQt4 import QtGui

class DeafWidget(QtGui.QWidget):
    ''' deaf widget 
    
    forwards events to parent 
    '''
    
    def setDeaf(self,status):
        if status:
            self._deaf = True
        else:
            self._deaf = False
    
    def getDeaf(self):
        return self._deaf
    
    def delDeaf(self):
        del self._deaf
        
    deaf = property(getDeaf,setDeaf,delDeaf,"A deaf button forwards events to its parent")
    
    def __init__(self):
        self.deaf = True
        
    def mouseMoveEvent(self, event):
        if self._deaf:
            event.ignore()

    def mousePressEvent(self, event):
        if self._deaf:
            event.ignore()
    
    def mouseReleaseEvent(self, event): 
        if self._deaf:
            event.ignore()
        
    def keyPressEvent(self,event):
        if self._deaf:
            event.ignore()

    def keyReleaseEvent(self,event):
        if self._deaf:
            event.ignore()