
import sys
import math
import time
from PyQt4 import QtCore, QtGui, Qt

from pyfittslaw.gui.widgets.fittslaw import FittsLawWidget, \
    buttonStyleSheetDeactive, buttonStyleSheetActive

class KnightRiderTargets(FittsLawWidget):

    """
    Knight rider target test
    """
    Orientations = ['horizontal', 'vertical']

    def __init__(self, *args, **kwargs):
        """

        arguments:
            parent - parent widget [QWidget]
            buttonCount - number of buttons
            buttonSize - size of buttons [QSize]
            buttonDistance - Euclidean distance between buttons
            logHandler - logging file handler (default:sys.stdout)
            sleepTime - time delay between interaction and showing the next target [s]
            sequenceMode - set to 'successor'
            hide_inactive - hide inactive buttons switch
            feedback_on_target_miss - show the user a target miss by updating the target (default:False)
            show_cursor -- mouse cursor visible flag (default:False)
            orientation -- widget orientation (InLineTargets.Orientations)
        """

        self._indexMode = 1 # increase or decrease index

        assert kwargs['orientation'] in KnightRiderTargets.Orientations, "invalid orientation"

        self.orientation = kwargs['orientation']
        del(kwargs['orientation'])

        kwargs['sequenceMode'] = "successor"
        super(KnightRiderTargets,self).__init__(*args,**kwargs)


    def refreshButtons(self):
        """
        refresh button view : arrange buttons clockwise in circle and show active button
        """

        centerOfMainWidget = QtCore.QPoint(self.width() / 2., self.height() / 2.)
        nButtons = len(self.buttons)

        # calculate positions
        position_x, position_y = [],[]
        if self.orientation == 'horizontal':
            offset_x = self.width() / (nButtons + 1)
            position_x = [offset_x * x for x in range(1,nButtons + 1)]
            position_y = [self.height() / 2.] * nButtons
        elif self.orientation == 'vertical':
            offset_y = self.height() / (nButtons + 2)
            position_x = [self.width() / 2.] * nButtons
            position_y = [offset_y * y for y in range(1,nButtons + 1)]
        else:
            raise Exception("invalid orientation")

        position = zip(position_x,position_y)

        for buttonIndex in range(len(self.buttons)):

            button = self.buttons[buttonIndex]
            button.resize(self._buttonSize)
            button.setStyleSheet(buttonStyleSheetDeactive)
            if self.hide_inactive_targets:
                button.hide()
            # adapt to center of button
            button.move(QtCore.QPoint(position[buttonIndex][0],position[buttonIndex][1]))

        # show active button
        self.activeButton.setStyleSheet(buttonStyleSheetActive)
        self.activeButton.show()

    def nextActiveButtonIndex(self):
        """
        returns the index of the next active button
        """
        nButtons = len(self.buttons)
        self._activeButtonIndex += self._indexMode
        self._activeButtonIndex %= nButtons

        # switch running direction
        if self._activeButtonIndex in [(nButtons - 1), 0]:
            self._indexMode *= -1

        return self._activeButtonIndex