import sys
import time
import numpy as np
from PyQt4 import QtGui,Qt,QtCore
from pyqtpc.gui import RealWorldFeedbackInterface

class Log(RealWorldFeedbackInterface, object):

    def __init__(self,log_handler = sys.stdout):
        super(Log,self).__init__()

        self.log_handler = log_handler

        self.init_dump()

    def init_dump(self):
        """
        initial log dump: column names
        """

        if self.log_handler:
            print >> self.log_handler, "#Timestamp " +\
                                  "Interaction_x Interaction_y Target_x Target_y " +\
                                  "PointingError_x PointingError_y TargetSize_x TargetSize_y hit"

    def dump(self,string):
        """
        log string to log handler
        """

        if self.log_handler:
            print >> self.log_handler, string


    def log_event(self, event):
        """
        evaluate and interaction

        arguments:
            event -- QMouseEvent
        """

        # evaluation
        interaction = event.globalPos()
        interactionPos = [interaction.x(), interaction.y()]

        # target
        target = self.targetAt(interactionPos)

        target_position = self.mapToGlobal(target.geometry().topLeft())
        target_center = self.targetFieldOfAttention(target)
        target_size = self.targetSize(target)

        # interaction error
        pointing_error = self.interactionError(interactionPos, target)
        # hit
        hit = self.hit(interactionPos, target)
        # log
        log_data = [str(time.time())] + interactionPos + target_center + pointing_error + target_size + [hit]
        log_string = "\t".join([str(x) for x in log_data])
        self.dump(log_string)
        


class LogWidget(QtGui.QWidget):
    ''' Log Widget
    
    logs interaction to log handler
    '''  
    def __init__(self, *args, **kwargs):
        ''' initialize log widget
        
        parameter
            parent - parent QWidget
            logHandler - logging handler
            logKeys - log keys as array of keys, that are logged
        '''
        if kwargs.has_key('logHandler'):
            self.log_handler = kwargs['logHandler']
            del kwargs['logHandler']
        else:
            self.log_handler = sys.stdout
        
        if kwargs.has_key('logKeys'):
            self.logKeys = kwargs['logKeys']
            del kwargs['logKeys']
        else:
            self.logKeys = ["unix time","click position","target position",
                            "absolute pointing error (from center)","miss","target"]
        
        QtGui.QWidget.__init__(self, *args, **kwargs) 

    def __str__(self):
        return self.__class__.__name__
    
    
    #@deprecated: implemented in QtGui.QWidget.mapToGlobal
    def globalPos(self):
        return self.mapToGlobal(self.pos())
         
    # on click handler if button hit
    
    def mousePressEvent(self, event):
        ''' mouse pressed:
        
            change style sheet to 'styleSheetPressed'
            
            emits LogButtonInfo with parameter log data 
            emits LogButtonPressed with parameter self.text()
        '''
        timeStamp = time.time() # take the time
        
        QtGui.QWidget.mousePressEvent(self, event)
        
        # target position is the center of the target widget
        target = event.target
        targetSize = QtCore.QPoint(target.width(),target.height())
        targetPosition = target.mapToGlobal(targetSize/2.)
        targetSizeArray = [targetSize.x(),targetSize.y()]
        # interaction
        interactionPosition = event.globalPos()
        
        # resulting pointing error
        pointingError = [interactionPosition.x() - targetPosition.x(), 
                             interactionPosition.y() - targetPosition.y()]
        nPointingError = np.array(pointingError, dtype=float) / np.array(targetSizeArray,dtype=float)
        
        if hasattr(self,"styleSheetPressed"):
            self.setStyleSheet(self.styleSheetPressed)
        
        logData = { "click position" : [event.globalPos().x(),event.globalPos().y()],
                   "target position" : [targetPosition.x(),targetPosition.y()],
                   "target size" : targetSizeArray,
                   "absolute pointing error (from center)" : pointingError,
                   "normalized pointing error" : nPointingError.tolist(),
                   "target class" : target.__class__.__name__,
                   "unix time": timeStamp,
                   "target" : str(target)
                   }
        
        if hasattr(event, "miss"):
            logData["miss"] = event.miss
        
        logDataCSV = []
        for key in iter(self.logKeys):
            if key in logData:
                logDataCSV.append(logData[key])

        logDataCSVString = reduce(lambda x,y: str(x) + "\t" + str(y), logDataCSV)
        if hasattr(self, "logHandler"):
            print >>self.log_handler, str(logDataCSVString)
            print "klsd"
        
