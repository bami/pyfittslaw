#!/usr/bin/python
''' MovingButtonWidget

    Copyright 2011 by Bastian Migge <miggeb@ethz.ch>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
from PyQt4 import QtGui, QtCore
from pyfittslaw.gui.widgets.button import *
from pyqtpc.gui.pyqt4.layout import FlowLayout

HyperLinkStyleSheet = '''
     QPushButton {
         border: 0px solid black;
         background-color: transparent;
         padding-bottom:1px;
         color: black;
         text-align: bottom left;
         text-decoration: underline;
         color : blue;
     }
'''

HyperLinkStyleSheetPressed = '''
     QPushButton {
         border: 0px solid black;
         background-color: transparent;
         padding-bottom:1px;
         color: black;
         text-align: bottom left;
         text-decoration: underline;
         color : gray;
     }
'''

bwStyleSheet = '''
        QPushButton {
            background-color: transparent;
            border: 1px solid black;
            text-align: middle center;
        }
        QPushButton:hover {
            background-color: transparent;
            }
        '''

class HyperTextWidget(QtGui.QWidget):
    ''' 
    HyperTextWidget
    
    Shows Text with underlined Hyperlinks, which beep on click.
    
    Log interaction as defined in LogButton
    
    '''
    def __init__(self, parent=None,
                 links=["ipsum", "seditor", "Ut", "ex", "consequat", "ersse", "deseriunt","frazilu"],
                 textBlocks=["Lorem", "dolor sit amet, consectetur adipisicing elit, ",
                             "do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                             "enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
                             "ex ea commodo, ", ". Duis aute irure dolor in reprehenderit in voluptate velit",
                             "cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
                             "mollit anim id est", "laborum."], 
                 cursor=QtCore.Qt.BlankCursor,
                 logHandler=sys.stdout):
        
        assert (len(links) + 1) == len(textBlocks), "invalid text" + str(len(links) + 1) + "/" + str(len(textBlocks))
        
        self.targets = []
        self.logHandler = logHandler
        
        self.parent = parent
        super(HyperTextWidget, self).__init__()
        
        #QtGui.QWidget.__init__(self, parent)
        layout = FlowLayout(parent=self)
        #layout = QtGui.QHBoxLayout(self)
                
        for index in range(len(links)):
            textItems = (str(textBlocks[index])).split(' ')
            for textItem in textItems:
                textLabel = QtGui.QLabel(textItem, parent=self)
                textLabel.setTextFormat(Qt.Qt.PlainText)
                layout.addWidget(textLabel)
                
            target = LogButton(links[index], parent=self, 
                               styleSheet=HyperLinkStyleSheet,
                               styleSheetPressed=HyperLinkStyleSheetPressed,
                               logHandler=self.logHandler)
            target.clicked.connect(QtGui.QApplication.beep)
            
            self.targets.append(target)
            layout.addWidget(target)
        
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'),
                     QtCore.SLOT('close()') )
        
        
        self.button = LogButton("Done", parent=self, logHandler=None, styleSheet=bwStyleSheet, styleSheetPressed=bwStyleSheet)
        layout.addWidget(self.button)
        self.connect(self.button, QtCore.SIGNAL('clicked()'), QtCore.SLOT('close()') )

        
        # widget setup
        self.setWindowTitle(str(self.__class__.__name__) + " - Hit ESC to exit")
        self.setCursor(cursor)
        self.show() #self.showMaximized()
        
    def __str__(self):
        return self.__class__.__name__
       
    def keyPressEvent(self, event):
        # exit on ESC
        if event.key() == QtCore.Qt.Key_Escape:
            self.emit(QtCore.SIGNAL('closeEmitApp()'))
            event.accept()
    
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    
    mw = HyperTextWidget()
    mw.show()
    
 # Groesse anpassen    

    mw.setGeometry(QtCore.QRect(0,0,400,280))
    
    
# Widget zentrieren 
   
    screen = QtGui.QDesktopWidget().screenGeometry()
    size =  mw.geometry() 
    mw.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)
        
    app.exec_()
    
