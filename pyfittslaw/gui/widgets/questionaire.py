#!/usr/bin/python
''' MovingButtonWidget

    Copyright 2011 by Bastian Migge <miggeb@ethz.ch>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
from PyQt4 import QtGui, QtCore
from pyfittslaw.gui.widgets.button import *
from pyfittslaw.gui.widgets.abtract import AbstractClickTestWidget

bwStyleSheet = '''
        QPushButton {
            background-color: transparent;
            border: 1px solid black;
            text-align: middle center;
        }
        QPushButton:hover {
            background-color: transparent;
            }
        '''

class QuestionaireWidget(AbstractClickTestWidget):

    def __init__(self,parent=None, 
                 question="question", 
                 answers=['lala','lulu','assas'],
                 cursor=QtCore.Qt.BlankCursor):
        self.parent = parent
        QtGui.QWidget.__init__(self, parent)
        layout = QtGui.QVBoxLayout(self)
        
        layout.addWidget(QtGui.QLabel(question))
        for text in answers:
            layout.addWidget(DeafQCheckBox(text,parent=self))            
        
        self.button = LogButton("Done", parent=self, logHandler=None, styleSheet=bwStyleSheet, styleSheetPressed=bwStyleSheet)
        layout.addWidget(self.button)
        self.connect(self.button, QtCore.SIGNAL('clicked()'), QtCore.SLOT('close()') )
        
        # widget setup
        self.setWindowTitle(str(self.__class__.__name__))
        self.setCursor(cursor)
        self.show() #self.showMaximized()
    
    def __str__(self):
        return self.__class__.__name__
    
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    
    mw = QuestionaireWidget()
    mw.show() 
    
# Groesse anpassen    

    mw.setGeometry(QtCore.QRect(0,0,280,400))
    
    
# Widget zentrieren 
   
    screen = QtGui.QDesktopWidget().screenGeometry()
    size =  mw.geometry() 
    mw.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)    
       
    app.exec_()
    
