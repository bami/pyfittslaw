# -*- coding: utf-8 -*-
"""
    pyfittslaw.gui.widgets.randoms
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
"""

from PyQt4 import QtCore, QtGui
import random
import time
import sys
import signal
from pyfittslaw.gui.widgets.log import Log
from pyfittslaw.gui.widgets.button import CallBackOnMouseButton
from pyqtpc.gui.pyqt4 import RealWorldFeedback

signal.signal(signal.SIGINT,signal.SIG_DFL) # Ctrl+C stops application

class PicCallbackButton(CallBackOnMouseButton):
    def __init__(self, pixmap, **kwargs):
        super(PicCallbackButton,self).__init__(**kwargs)

        self.pixmap = pixmap

    def paintEvent(self, event):
        qpainter = QtGui.QPainter(self)
        rect = QtCore.QRect(event.rect())

        rect.setHeight(event.rect().height())
        rect.setWidth(event.rect().width())
        rect.moveTo(0,0)
        qpainter.drawPixmap(rect, self.pixmap)


class MovingTargets(QtGui.QWidget, Log, RealWorldFeedback):
    """
    Random moving target widget
    """
    def __init__(self, background_image_filename, image_file_names, target_sizes = [[10,10],[20,20]], 
        show_cursor = True, feedback_on_target_miss = True, **kwargs):

        QtGui.QWidget.__init__(self,**kwargs)
        Log.__init__(self,**kwargs)
        RealWorldFeedback.__init__(self,**kwargs)

        self.setCursor(QtGui.QCursor(QtCore.Qt.BlankCursor))

        self.show_cursor = show_cursor
        self.feedback_on_target_miss = feedback_on_target_miss

        self.cursor_x, self.cursor_y = [0,0]
        self.pic = QtGui.QLabel(self)
        self.pic.setScaledContents(True)
        self.pic.setPixmap(QtGui.QPixmap(background_image_filename))
        self.pic.setGeometry(0, 0, self.width(), self.height()) 
        
        self.targets = []
        # create image buttons in sizes
        for image_file_name in image_file_names:
            for target_size in target_sizes:
                pixmap = QtGui.QPixmap(image_file_name)
                button = PicCallbackButton(parent=self, pixmap = pixmap, onMouseReleaseCallback=self.log_and_update)
                button.hide()
                button.resize(QtCore.QSize(target_size[0],target_size[1]))
                button.clicked.connect(self.update_target)
                self.targets.append(button)

        self.target = self.targets[0]
        self.update_target()

    def keyPressEvent(self, QKeyEvent):
        self.update_target()

    def resizeEvent(self, QResizeEvent):
        self.pic.setGeometry(0, 0, self.width(), self.height()) 

    def paintEvent(self, event):
        if self.show_cursor:
            radius = 4
            brush = QtGui.QBrush(QtCore.Qt.yellow)
            qp = QtGui.QPainter()
            qp.begin(self)
            qp.setBrush(brush)
            qp.drawEllipse(self.cursor_x - radius, self.cursor_y - radius, 2*radius, 2*radius)
            qp.end()


    def mousePressEvent(self, event):
        """
        ignore button press events in application logic, update cursor, if shown
        """
        self.update_cursor(event)

    
    def mouseReleaseEvent(self, event):
        """
        if no feedback_on_target_miss, process
        update cursor, if shown
        """

        if self.feedback_on_target_miss:
            self.log_event(event)
        else:
            self.log_and_update(event)

        self.update_cursor(event)

    def mouseMoveEvent(self, event):
        """
        update cursor, if shown
        """
        self.update_cursor(event)


    def update_cursor(self,event):
        self.cursor_x = event.x()
        self.cursor_y = event.y()
        self.update()


    def log_and_update(self,event):
        self.log_event(event)
        self.update_target()

    def update_target(self):
        self.target.hide()
        # select target randomly
        index = random.randint(0,len(self.targets)-1)
        self.target = self.targets[index]

        # set position
        xPos = random.random() * (self.width() - self.target.width())
        yPos = random.random() * (self.height() - self.target.height())

        self.target.setGeometry(xPos,
            yPos,
            self.target.width(),
            self.target.height())

        self.target.show()

    def targetAt(self, position):
        """
        return self

        arguments:
            position - global window coordinates [list]
        """
        return self.target

    def targetFieldOfAttention(self, target):
        '''
        return center of self

        arguments
            target - [QWidget, QFrame]
        '''
        geometry = target.geometry()

        center_Qt = QtCore.QPoint(geometry.x() + geometry.width()/2., geometry.y() + geometry.height()/2.)
        center_global_Qt = self.mapToGlobal(center_Qt)
        center = [center_global_Qt.x(),center_global_Qt.y()]
        return center


def main():
    background_image_filename = "/home/bami/git/pyfittslaw/pyfittslaw/res/jungle.jpg"
    image_file_names = ["/home/bami/git/pyfittslaw/pyfittslaw/res/apple.png"]
    target_sizes = [[10,10],[20,20],[40,40],[60,60]]
    app = QtGui.QApplication(sys.argv)
    w = MovingTargets(background_image_filename = background_image_filename,
        image_file_names = image_file_names,
        target_sizes = target_sizes)
    w.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
