#!/usr/bin/python
''' MovingButtonWidget

    Copyright 2011 by Bastian Migge <miggeb@ethz.ch>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
import sys
import sip
import time
import random
from PyQt4 import QtGui, QtCore
from pyfittslaw.gui.widgets.button import *
from pyfittslaw.gui.widgets.abtract import AbstractClickTestWidget

class MovingButtonWidget(AbstractClickTestWidget):
    ''' widget that shows a single button. 
    
        the button moves randomly (uniform distributed) triggered by an interaction.
        the widget is shown full screen with disabled cursor feedback
        
        keys:
        * space bar is interpreted as click
        * ESC closes the widget
    '''
    def get_activeButtonIndex(self):
        pass
    
    def set_activeButtonIndex(self,buttonIndex):
        pass
    
    def get_activeButton(self):
        return self.button
    
    activeButton = property(get_activeButton)

    def get_button(self):
        return self._button

    def set_button(self,button):
        # set button and connect it to call slef.updateTarget on click
        if hasattr(self, "_button") and self._button != None: 
            self._button.hide()
        
        self._button = button

        if self._button != None:
            self._button.show()
            # DO NOT USE THIS QT SIGNAL SHIT !!!! 
            # self._button.clicked.connect(self.updateTarget)
            # self.connect(self._button, QtCore.SIGNAL('LogButtonPressed'),self.updateTarget)
        
    button = property(get_button,set_button)

    def __init__(self,
                 button,
                 parent = None,
                 logHandler = sys.stdout,
                 considerTargetMiss = False,
                 cursor = QtCore.Qt.BlankCursor):

        self.logHandler = logHandler
        self.parent = parent
        self._considerTargetMiss = considerTargetMiss
        
        QtGui.QWidget.__init__(self, parent)
        
        self.button = button 
        
        # close app signal connect (see keyPressEvent())
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'), QtCore.SLOT('close()') )
        
        # widget setup
        self.setWindowTitle(str(self.__class__.__name__) + " - Hit ESC to exit")
        self.setCursor(cursor) # set cursor (e.g.QtCore.Qt.arrowCursor)
        self.showMaximized()
       
        return
    
    def __str__(self):
        return self.__class__.__name__
            
    def updateTarget(self):
        if isinstance(self.button, Qt.QWidget):
            newPosition = self.__getNextButtonPosition()
            self.button.setGeometry(newPosition.x(),newPosition.y(),self.button.width(),self.button.height())

    def __getNextButtonPosition(self):
        ''' calculate the next button position 
        sampling a probability density function
        
        in this case uniform distributed
        ''' 
        xPos = random.random() * (self.width() - self.button.width())
        yPos = random.random() * (self.height() - self.button.height())
        return QtCore.QPoint(xPos,yPos)

class MultipleMovingButtonWidget(MovingButtonWidget):
    def __init__(self, counterLimit, parent=None,logHandler=sys.stdout, considerTargetMiss=False,cursor=QtCore.Qt.BlankCursor):
        
        super(MultipleMovingButtonWidget,self).__init__(parent, logHandler=logHandler, considerTargetMiss=considerTargetMiss,cursor=cursor)
        
        self._counterLimit = counterLimit
        self._buttons = []
    
    def __str__(self):
        return self.__class__.__name__
    
    def get_buttons(self):
        return self._buttons
    
    def set_buttons(self,buttons):
        for button in self._buttons:
            sip.delete(self._button)
            
        self._buttons = [] 
        self._shownCount = [self._counterLimit] * len(buttons)
        
        for button in buttons:
            button.setParent(self)
            button.hide()
            self._buttons.append(button)
        self.updateTarget()
        
    buttons = property(get_buttons,set_buttons)
    
    def nextButton(self):          
        # select new button randomly 
        # exit if no button candidates left
        
        buttonCount = len(self.buttons)
        if buttonCount > 1:
            index = random.randint(0,buttonCount-1)
        elif buttonCount == 1: 
            index = 0
        else:
            return None
        
        self._shownCount[index] -= 1 # decrease shown counter 
        button = self._buttons[index]
        
        # remove button, if counter limit reached
        if self._shownCount[index] <= 0:
            self.buttons.pop(index)  
            self._shownCount.pop(index)
        return button
            
    def updateTarget(self):
        '''
        get next target from buttons 
        if no target left, close gui
        '''
        
        self.button = self.nextButton()
        
        if self.button == None:
            self.emit(QtCore.SIGNAL('closeEmitApp()'))
        
        super(MultipleMovingButtonWidget,self).updateTarget()
                    
    def mousePressEvent(self, event):
        
        # different logging style
        logDataCSV = []
        logDataCSV.append(time.time())
        logDataCSV.append(self)
        logDataCSV.append(self.parent)
        logDataCSV.append('targetMiss') 
        logDataCSV.extend([event.globalPos().x(),event.globalPos().y()])

        logDataCSVString = reduce(lambda x,y: str(x) + "\t" + str(y), logDataCSV)
        print >> self.logHandler, logDataCSVString
        
        if self._considerTargetMiss:
            self.updateTarget()
            super(MultipleMovingButtonWidget,self).mousePressEvent(event)
        else:
            pass
                           
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    
    mw = MultipleMovingButtonWidget(2)
    
    #button = LabelPicButton(QtGui.QPixmap("/usr/lib/rhythmbox/plugins/umusicstore/musicstore_icon.png"),parent=mw)
    buttons = [EllipseButton(parent=mw,show=False),
                      PolygonButton(parent=mw,show=False,points=[QtCore.QPointF(0., 0.), 
                      QtCore.QPointF(1., 0.), 
                      QtCore.QPointF(1., 1.),
                      QtCore.QPointF(0., 1.)])
                ]
    
    mw.buttons = buttons
    
    mw.show()   
    #button.show() 
    app.exec_()
