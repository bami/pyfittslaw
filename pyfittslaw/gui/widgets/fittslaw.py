# -*- coding: utf-8 -*-
"""
    pyfittslaw.gui.widget.fittslaw
    ~~~~~~

    :copyright: (c) 2010, 2012 by Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: Fitts Law Click test
    Implementation of the Fitts Law Click test with parallax error correction
"""

import sys
import math
import time
from PyQt4 import QtCore, QtGui, Qt

from pyfittslaw.gui.widgets.abtract import AbstractClickTestWidget
from pyfittslaw.gui.widgets.log import Log
from pyqtpc.gui.pyqt4 import RealWorldFeedback
from pyfittslaw.gui.widgets.button import CallBackOnMouseButton

buttonStyleSheetActive = '''
     QPushButton {
         border: 0px solid red;
         background-color: red;
         color: white;
         text-align: center;
     }
     QPushButton:hover { background-color: red; }
     QPushButton:focus { }
'''

buttonStyleSheetDeactive = '''
     QPushButton {
         border: 0px solid gray;
         background-color: gray;
         color: gray;
         text-align: center;
     }
     QPushButton:hover { }
     QPushButton:focus { }
'''

buttonStyleSheetActiveFeedback = '''
     QPushButton {
         border: 0px solid red;
         background-color: red;
         color: white;
         text-align: center;
     }
     QPushButton:hover { background-color: black; }
     QPushButton:focus { }
'''

buttonStyleSheetDeactiveFeedback = '''
     QPushButton {
         border: 0px solid gray;
         background-color: gray;
         color: gray;
         text-align: center;
     }
     QPushButton:hover { background-color: black; }
     QPushButton:hover { }
     QPushButton:focus { }
'''


def pixTomm(list,pixelpitch):
    return [t*c for t,c in zip(list,pixelpitch)]

def rotate(position,alpha_x):
    """
    rotate position around first dimension

    arguments:
        position - 3d position vector
        alpha_x - rotation angle in
    """
    rotated_pos= [None] * 3
    rotated_pos[0] = position[0]
    rotated_pos[1] = math.cos(alpha_x)*position[1]-math.sin(alpha_x)*position[2]
    rotated_pos[2] = math.sin(alpha_x)*position[1]+math.cos(alpha_x)*position[2]

    return rotated_pos

class FittsLawWidget(AbstractClickTestWidget, QtGui.QWidget, Log, RealWorldFeedback):
    """
    Fitts' Law Circle click widget

    the applications show a sequence of targets aligned in a circle. The actual target is indicated by its style
    (buttonStyleSheetActive). The inactive targets can be set invisible.
    Feedback to the user in terms of moving to the next target only if he hit it is supported.
    Hovering feedback is implemented by the button style CSS.
    The order modus of showing the next target is given by FittsLawWidget.sequenceModi. It is either the opponent or
    the clockwise successor of the current target.
    The cursor is hidden by default
    Widget exits on ESC.
    on pointer-release the interaction the following data is logged: target position, pointing position, interaction error

    """

    ### static stuff ###

    sequenceModi = ["successor", "opponent"]
    
    @staticmethod
    def indexOfPerformance(movementTimeToCompleteTask, indexOfDifficulty):
        ''' 
        IP - Index of performance, w.r.t. Fitts' Law
        
        arguments:
            movementTimeToCompleteTask
            indexOfDifficulty
        ''' 
        return float(indexOfDifficulty) / float(movementTimeToCompleteTask)
    
    @staticmethod
    def approachAngle(sourceTarget, sinkTarget):
        '''
        angle of approaching sinkTarget from sourceTarget
        
        arguments:
            sourceTarget - source target [QWidget]
            sinkTarget - sink target [QWidget]
        '''
        
        sinkTargetGeomerty = sinkTarget.geometry()
        sinkTargetPos = sinkTarget.parent.mapToGlobal(sinkTargetGeomerty.topLeft())
        sinkTargetCenterPos = QtCore.QPoint(sinkTargetPos.x() + sinkTargetGeomerty.width() / 2,
                                        sinkTargetPos.y() + sinkTargetGeomerty.height() / 2)
        
        sourceTargetGeomerty = sourceTarget.geometry()
        sourceTargetPos = sourceTarget.parent.mapToGlobal(sourceTargetGeomerty.topLeft())
        sourceTargetCenterPos = QtCore.QPoint(sourceTargetPos.x() + sourceTargetGeomerty.width() / 2,
                                        sourceTargetPos.y() + sourceTargetGeomerty.height() / 2)
        
        offsetX = float(sourceTargetCenterPos.x() - sinkTargetCenterPos.x())
        offsetY = float(sourceTargetCenterPos.y() - sinkTargetCenterPos.y())
        
        return math.tan(offsetX / offsetY)

    ### properties ###
    def get_buttonSize(self):
        return self._buttonSize
    
    def set_buttonSize(self, QtCore_QSize_size):
        self._buttonSize = QtCore_QSize_size
        self.refreshButtons()
    
    buttonSize = property(get_buttonSize, set_buttonSize)
    
    def get_buttonDistance(self):
        return self._buttonSize
    
    def set_buttonDistance(self, distance):
        self._buttonDistance = distance
        self.refreshButtons()
    
    buttonDistance = property(get_buttonDistance, set_buttonDistance)
    
    def get_activeButtonIndex(self):
        return self._activeButtonIndex
    
    def set_activeButtonIndex(self, buttonIndex):
        self._activeButtonIndex = buttonIndex
        
    activeButtonIndex = property(get_activeButtonIndex, set_activeButtonIndex)
    
    def get_activeButton(self):
        return self.buttons[self.activeButtonIndex]
    
    activeButton = property(get_activeButton)
    
    def get_activeButtonPredecessor(self):
        return self._lastButtonIndex
    
    activeButtonPredecessor = property(get_activeButtonPredecessor)

    def __init__(self,
                 parent = None,
                 buttonCount = 2,
                 buttonSize = QtCore.QSize(8, 30),
                 buttonDistance = 30.,
                 logHandler = sys.stdout,
                 sequenceMode = "opponent",
                 hide_inactive = False,
                 feedback_on_target_miss = False,
                 show_cursor = False):
        ''' 
        initialize FittsLawWidget
        
        arguments:
            parent - parent widget [QWidget]
            buttonCount - number of buttons 
            buttonSize - size of buttons [QSize]
            buttonDistance - Euclidean distance between buttons  
            logHandler - logging file handler (default:sys.stdout)
            sleepTime - time delay between interaction and showing the next target [s]
            sequenceMode - sequence mode (FittsLawWidget.sequenceModi)
            hide_inactive - hide inactive buttons switch
            feedback_on_target_miss - show the user a target miss by updating the target (default:False)
            show_cursor -- mouse cursor visible flag (default:False)
        '''
        
        # sanity Check
        assert buttonSize.__class__ != QtCore.QSize.__class__, "wrong format"
        assert sequenceMode in FittsLawWidget.sequenceModi, "sequenceMode invalid"

        self.log_handler = logHandler
        self.feedback_on_target_miss = feedback_on_target_miss
        self.hide_inactive_targets = hide_inactive
        self.sequenceMode = sequenceMode
        self._buttonSize = buttonSize
        self._buttonDistance = buttonDistance
        self.buttons = []
        self._activeButtonIndex = 0
        self._lastButtonIndex = 0

        QtGui.QWidget.__init__(self, parent = parent)
        Log.__init__(self, logHandler) 
        RealWorldFeedback.__init__(self)

        # close app signal connect (see keyPressEvent())
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'), QtCore.SLOT('close()'))

        # add buttons
        for buttonIndex in range(buttonCount):
            button = CallBackOnMouseButton(self, onMouseReleaseCallback=self.log_and_update)
            
            button.setStyleSheet(buttonStyleSheetDeactive)
            button.setFocusPolicy(Qt.Qt.NoFocus) # do not show dotted line around button
            
            if self.hide_inactive_targets:
                button.hide()
            
            self.buttons.append(button)

        self.show_cursor = show_cursor
        self.cursor_x = 0
        self.cursor_y = 0

        # widget setup
        self.setCursor(QtGui.QCursor(QtCore.Qt.BlankCursor))

        self.showMaximized()
        self.setWindowTitle(str(self.__class__.__name__))

        self.log_handler = logHandler

        self.refreshButtons()
        
        return

    def nextActiveButtonIndex(self):
        """
        returns the index of the next active button
        """

        self._lastButtonIndex = self.activeButtonIndex
        
        if self.sequenceMode == self.sequenceModi[0]:
            self._activeButtonIndex += 1
        elif self.sequenceMode == self.sequenceModi[1]:
            self._activeButtonIndex += (len(self.buttons) / 2) + 1
        
        self._activeButtonIndex %= len(self.buttons)
        
        return self.activeButtonIndex
    
    def indexOfDifficulty(self):
        """
        return ID; the Fitts' Law Index of difficulty
        """

        avgWidth = 0.5 * (self.buttonSize[0] + self.buttonSize[1])
        
        fittsID = math.log(2, 2. * self.buttonDistance / avgWidth)
        welfordID = math.log(2, self.buttonDistance / avgWidth + 0.5)
        macKenzieID = math.log(2, self.buttonDistance / avgWidth + 1.0)
        
        return macKenzieID
    
    def refreshButtons(self):
        """
        refresh button view : arrange buttons clockwise in circle and show active button
        """
        
        centerOfMainWidget = QtCore.QPoint(self.width() / 2., self.height() / 2.)
        stepWidthAngle = 2.*math.pi / len(self.buttons)
        radius = self._buttonDistance
        
        angle = math.pi / 2.
        for button in self.buttons:
            button.resize(self._buttonSize)
            button.setStyleSheet(buttonStyleSheetDeactive)
            if self.hide_inactive_targets:
                button.hide() 
            delta = QtCore.QPoint(math.sin(angle) * radius, math.cos(angle) * radius * -1.)
            position = centerOfMainWidget + delta 
            position -= QtCore.QPoint(button.width() / 2., button.height() / 2.) # center vs. upper left corner
            # adapt to center of button
            button.move(position)
            angle += stepWidthAngle
            
        # show active button
        self.activeButton.setStyleSheet(buttonStyleSheetActive)
        self.activeButton.show()
            
    def updateTarget(self):
        """
        show next button: set button index and refresh view
        """

        self.nextActiveButtonIndex()
        self.refreshButtons()

    def paintEvent(self, event):
        if self.show_cursor:
            radius = 4
            brush = QtGui.QBrush(QtCore.Qt.black)
            qp = QtGui.QPainter()
            qp.begin(self)
            qp.setBrush(brush)
            qp.drawEllipse(self.cursor_x - radius, self.cursor_y - radius, 2*radius, 2*radius)
            qp.end()


    def mousePressEvent(self, event):
        """
        ignore button press events in application logic, update cursor, if shown
        """
        self.update_cursor(event)

    
    def mouseReleaseEvent(self, event):
        """
        if no feedback_on_target_miss, process
        update cursor, if shown
        """

        if self.feedback_on_target_miss:
            self.log_event(event)
        else:
            self.log_and_update(event)

        self.update_cursor(event)

    def mouseMoveEvent(self, event):
        """
        update cursor, if shown
        """
        self.update_cursor(event)


    def update_cursor(self,event):
        self.cursor_x = event.x()
        self.cursor_y = event.y()
        self.update()

    def log_and_update(self,event):
        """
        log the interaction event and update the target
        """
        self.log_event(event)
        self.updateTarget()

    def targetAt(self, position):
        """
        return self

        arguments:
            position - global window coordinates [list]
        """
        return self.activeButton


    def targetFieldOfAttention(self, target):
        '''
        return center of self

        arguments
            target - [QWidget, QFrame]
        '''
        geometry = target.geometry()

        center_Qt = QtCore.QPoint(geometry.x() + geometry.width()/2., geometry.y() + geometry.height()/2.)
        center_global_Qt = self.mapToGlobal(center_Qt)
        center = [center_global_Qt.x(),center_global_Qt.y()]
        return center

class FittsLawWidgetUserTracker(FittsLawWidget):
    """
    Fitts' Law interaction widget with viewpoint tracker

    in contrast to the FittsLawWidget, this class logs all data 3-dimensional in [mm]

    It is used for modeling the coherence between interaction error and relative viewpoint int the observation
    model of the correction controller

    """
    def __init__(self,
                 user_tracker,
                 parent = None,
                 buttonCount = 2,
                 buttonSize = QtCore.QSize(8, 30),
                 buttonDistance = 30.,
                 logHandler = sys.stdout,
                 sequenceMode = "opponent",
                 hide_inactive = False,
                 feedback_on_target_miss = False):

        self.screenOffset = int(10./0.808)
        self.pixel_pitch_3d = [1.0725,0.808,1]
        self.pixel_pitch_2d = [1.0725,0.808]
        self.camera_position = [549,-275,-90]
        self.display_rotation_x=-0.25716

        super(FittsLawWidgetUserTracker,self).__init__(parent=None,
            buttonCount= buttonCount,
            buttonSize=buttonSize,
            buttonDistance=buttonDistance,
            logHandler=logHandler,
            sequenceMode=sequenceMode,
            hide_inactive=hide_inactive,
            feedback_on_target_miss = feedback_on_target_miss
        )

        self.user_tracker = user_tracker

    def init_dump(self):
        """
        initial log dump: column names
        """

        if self.log_handler:
            print >> self.log_handler, "#Timestamp " +\
                                      "Interaction_x Interaction_y Interaction_z Target_x Target_y Target_z" +\
                                      "PointingError_x PointingError_y Viewpoint_x Viewpoint_y Viewpoint_z TargetSize_x TargetSize_y"

    def log_event(self, event):
        """
        process:
            * evaluate interaction in 3d
            * log viewpoint in 3d
            * log evaluation [mm]
            * update target
        """

        # evaluation
        interaction = event.globalPos()
        interactionPos_px = [interaction.x(), interaction.y(),self.screenOffset]
        interactionPos=pixTomm(interactionPos_px,self.pixel_pitch_3d)

        target_geometry = self.activeButton.geometry()
        target_position = self.mapToGlobal(target_geometry.topLeft())

        target_center_point = QtCore.QPoint(target_position.x() + target_geometry.width() / 2,
            target_position.y() + target_geometry.height() / 2)

        target_center_3d_px = [target_center_point.x(), target_center_point.y(), 0]
        target_center_3d=pixTomm(target_center_3d_px,self.pixel_pitch_3d)

        target_size_px = [target_geometry.width(),target_geometry.height()]
        target_size = pixTomm(target_size_px,self.pixel_pitch_2d)

        pointing_error_px = [interaction.x() - target_center_point.x(),
                             interaction.y() - target_center_point.y()]
        pointing_error = pixTomm(pointing_error_px,self.pixel_pitch_2d)

        viewpoint = self.user_tracker.getViewpoint()

        viewpoint[1] = -viewpoint[1] # swap y axis
        voiwpoint_display_mm = [t+c for t,c in zip(viewpoint,self.camera_position)] # move to display origin
        #x axis rotation
        voiwpoint_display_mm=rotate(voiwpoint_display_mm,self.display_rotation_x)

        # log
        log_data = [str(time.time())] + interactionPos + target_center_3d + pointing_error +\
                   voiwpoint_display_mm + target_size
        log_string = "\t".join([str(x) for x in log_data])
        self.dump(log_string)

        # update target
        self.updateTarget()

if __name__ == "__main__":
    
    app = QtGui.QApplication(sys.argv)    
    
    mw = FittsLawWidget(buttonCount=20,
                        buttonSize=QtCore.QSize(20, 20),
                        buttonDistance=200,
                        show_cursor = True)
    mw.show()  
    sys.exit(app.exec_())  
    
