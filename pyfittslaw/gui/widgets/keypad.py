#!/usr/bin/python
''' MovingButtonWidget

    Copyright 2011 by Bastian Migge <miggeb@ethz.ch>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
import math
from PyQt4 import QtGui, QtCore
from pyfittslaw.gui.widgets.button import *

bwBorderTopLeftStyleSheet = '''
     QPushButton {
         border: 1px solid black;
         background-color: transparent;
         color: black;
         text-align: top left;
     }
'''

bwBorderBottomRightStyleSheet = '''
     QPushButton {
         border: 1px solid black;
         background-color: transparent;
         color: black;
         text-align: bottom right;
     }
'''

bwBorderCenterStyleSheet = '''
     QPushButton {
         border: 1px solid black;
         background-color: transparent;
         color: black;
         text-align: center center;
     }
'''

class KeyPadWidget(QtGui.QWidget):

    def __init__(self,parent=None, logHandler = sys.stdout, 
                 buttonSize=QtCore.QSize(50,50),
                 spacing=14, 
                 buttonStyle=bwBorderCenterStyleSheet,
                 cursor=QtCore.Qt.BlankCursor,
                 displaxText="inspire ICVR"):
        
        super(KeyPadWidget, self).__init__()
        self.logHandler = logHandler
        self._buttonSize = buttonSize
        self._buttonStyle = buttonStyle
        self._spacing = spacing
        self._displayText = displaxText
        self.initUI()
        
        # window setup
        self.setWindowTitle(str(self.__class__.__name__) + " - Hit ESC to exit")
        self.setCursor(cursor) # disable cursor feedback
        self.show() #self.showMaximized()
        
        
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'), 
                     QtCore.SLOT('close()') )
    
    def __str__(self):
        return self.__class__.__name__
        
    def initUI(self):
        # buttons
        names = ['Cls', 'Back', '', 'Exit', '7', '8', '9', '/',
            '4', '5', '6', '*', '1', '2', '3', '-',
            '0', '.', '=', '+']

        gridLayout = QtGui.QGridLayout()
        
        self._display = QtGui.QLineEdit(parent=self)
        self._display.setText(self._displayText)
        gridLayout.addWidget(self._display,5,0,1,5)
        j = 0
        pos = [(0, 0), (0, 1), (0, 2), (0, 3),
                (1, 0), (1, 1), (1, 2), (1, 3),
                (2, 0), (2, 1), (2, 2), (2, 3),
                (3, 0), (3, 1), (3, 2), (3, 3 ),
                (4, 0), (4, 1), (4, 2), (4, 3)]


        for i in names:
            #button = QtGui.QPushButton(i)
            button = LogButton(i, parent=self, 
                               styleSheet=self._buttonStyle,
                               styleSheetPressed=self._buttonStyle,
                               logHandler=self.logHandler)
            button.setMinimumSize(self._buttonSize.height(), self._buttonSize.width())
            button.resize(self._buttonSize)
            if (i == 'Exit'):
                self.connect(button, QtCore.SIGNAL('clicked()'),
                              QtCore.SLOT('close()') )
            elif (i == 'Cls'):
                self.connect(button, QtCore.SIGNAL('clicked()'),
                             self._clearDisplay)
            elif (i == 'Back'):
                self.connect(button, QtCore.SIGNAL('clicked()'),
                             self._backspaceDisplay)
            else:
                self.connect(button, Qt.SIGNAL("LogButtonPressed"),
                             self._addTextToDisplay)
                
            if not len(i):
                gridLayout.addWidget(QtGui.QLabel(''), pos[j][0], pos[j][1])
                button.hide() # HACK 
                button = None
            else: 
                gridLayout.addWidget(button, pos[j][0], pos[j][1])
            j = j + 1

        #gridLayout.setRowStretch(0,0)
        #gridLayout.setColumnStretch(0,0)
        
        gridLayout.setRowStretch(gridLayout.rowCount(),1)
        gridLayout.setColumnStretch(gridLayout.columnCount(),1)
        gridLayout.setVerticalSpacing(self._spacing)
        gridLayout.setHorizontalSpacing(self._spacing)

        self.setLayout(gridLayout)
    
    def _addTextToDisplay(self,*args):
        self._display.setText(self._display.text().append(args[0]))
    
    def _clearDisplay(self):
        self._display.clear()
    
    def _backspaceDisplay(self):
        self._display.backspace()
    
    def mouseMoveEvent(self, event):
        distance = float("inf")
        
        if not hasattr(self, "targets"):
            return
        
        for target in self.targets:
            targetDistance = math.sqrt((event.x() - target.x())**2 + (event.y() - target.y())**2)
            if targetDistance < distance:
                distance = targetDistance
                closestTarget = target
        
        self.activeButton = target
        
        event.ignore()
        
    def keyPressEvent(self, event):
        # exit on ESC
        if event.key() == QtCore.Qt.Key_Escape:
            self.emit(QtCore.SIGNAL('closeEmitApp()'))
            event.accept()
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    
    mw = KeyPadWidget()
    mw.show()
    
# Groesse anpassen    

    mw.setGeometry(QtCore.QRect(0,0,280,400))
    
    
# Widget zentrieren 
   
    screen = QtGui.QDesktopWidget().screenGeometry()
    size =  mw.geometry() 
    mw.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)

    app.exec_()
    