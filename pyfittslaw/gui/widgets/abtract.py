#!/usr/bin/python
''' Click Test Class 

    Copyright 2010 by Bastian Migge <miggeb@ethz.ch>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''


from PyQt4 import QtGui, QtCore, Qt

class AbstractClickTestWidget(QtGui.QWidget):
    """
    Widget exits on ESC
    """
    
    def get_activeButtonIndex(self):
        raise NotImplementedError()
    
    def set_activeButtonIndex(self,buttonIndex):
        raise NotImplementedError()
    
    activeButtonIndex = property(get_activeButtonIndex,set_activeButtonIndex)
    
    def get_activeButton(self):
        raise NotImplementedError()
    
    activeButton = property(get_activeButton)
    
    def updateTarget(self):
        raise NotImplementedError()
    
    def refreshButtons(self):
        raise NotImplementedError()
    
    def resizeEvent(self,event):
        self.refreshButtons()
        
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.emit(QtCore.SIGNAL('closeEmitApp()'))
            event.accept()
        elif event.key() == QtCore.Qt.Key_Space:
            self.updateTarget()
        else:
            event.ignore()

    def mousePressEvent(self, event):
        '''
        on click handler if button passed event
        '''
        raise NotImplementedError()
