#!/usr/bin/python
''' 
MovingButton
    Copyright 2010-2011 by Bastian Migge <miggeb@ethz.ch>

    Changes position after click event asking it's parent
    (*parent.getNextTargetPosition(currentClickPosition)*)

License

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    
'''

import sys
import time
import math
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
from pyfittslaw.gui.widgets.log import LogWidget
from pyfittslaw.gui.widgets.deaf import DeafWidget

# standard style sheets
bwStyleSheetBorderless = '''
     QPushButton {
         border: 0px solid black;
         background-color: transparent;
         color: black;
         text-align: bottom left;
     }
     QPushButton:hover { background-color: green; }
     QCheckBox:hover { background-color: transparent; }
     QRadioButton:hover { background-color: transparent; }
'''

bwStyleSheetBorder = '''
     QPushButton {
         border: 1px solid black;
         background-color: transparent;
         color: black;
         text-align: bottom left;
     }
     QPushButton:hover { background-color: green; }
     QCheckBox:hover { background-color: transparent; }
     QRadioButton:hover { background-color: transparent; }
'''


bwStyleSheetPressed = '''
     QPushButton {
         border: 0px solid black;
         background-color: transparent;
         color: gray;
         text-align: bottom left;
     }
     QPushButton:hover { background-color: blue; }
'''

class ButtonFactory(object):
    ''' factory by object name '''
    
    @staticmethod
    def newButton(objecttypeStr):
        ''' create object by class name '''
        for ObjectClass in LogButton.__subclasses__():
            if ObjectClass.__name__ == objecttypeStr:
                return ObjectClass()
        #if research was unsuccessful, raise an error
        raise ValueError('No ObjectClass containing "%s".' % objecttypeStr)
    

class LogButton(QtGui.QPushButton,LogWidget):
    ''' LogButton 
            log click position and click error 
            
            *passEventToParent* 
            
    '''

    @classmethod
    def getDefault(*args,**kwargs):
        ''' return a default style button '''
        button = LogButton(*args,**kwargs)
        button.setStyleSheet(bwStyleSheetBorderless)
        button.styleSheetPressed(bwStyleSheetBorderless)
        button.setFocusPolicy(Qt.Qt.NoFocus) # do not show dotted line around button
        button.show()
        return button
    
    def __init__(self, *args,**kwargs):
        ''' init

        arguments:
            parent - parent QWidget
        '''
        
        super(LogButton,self).__init__(*args,**kwargs)
        self.setFocusPolicy(Qt.Qt.NoFocus)
        
        
    def mousePressEvent(self, event):
        event.miss = False
        event.target = self
        
        super(LogButton,self).mousePressEvent(event)
        event.accept()
        
class CallBackOnMouseButton(QtGui.QPushButton):
    
    ''' calls super class on MousePress'''
    def __init__(self, *args, **kwargs):
        ''' init
        
        arguments
            onMousePressCallback - name of the callback function
            onMouseMoveCallback - name of the callback function
            onMouseReleaseCallback - name of the callback function
        '''
        if kwargs.has_key("onMousePressCallback"):
            self.onMousePressCallback = kwargs['onMousePressCallback']
            del kwargs['onMousePressCallback']
        
        if kwargs.has_key("onMouseMoveCallback"):
            self.onMouseMoveCallback = kwargs['onMouseMoveCallback']
            del kwargs['onMouseMoveCallback']
        
        if kwargs.has_key("onMouseReleaseCallback"):
            self.onMouseReleaseCallback = kwargs['onMouseReleaseCallback']
            del kwargs['onMouseReleaseCallback']
        
        super(CallBackOnMouseButton, self).__init__(*args, **kwargs)
        
    def mousePressEvent(self, event):
        if hasattr(self,'onMousePressCallback'):
            self.onMousePressCallback(event)
        else:
            event.ignore() 
    
    def mouseMoveEvent(self, event):
        if hasattr(self,'onMouseMoveCallback'):
            self.onMouseMoveCallback(event)
        else:
            event.ignore() 
    
    def mouseReleaseEvent(self, event):
        if hasattr(self,'onMouseReleaseCallback'):
            self.onMouseReleaseCallback(event)
        else:
            event.ignore() 
        
        
class StickyButton(QtGui.QWidget):
    ''' LogButton 
            log click position and click error 
            
            *passEventToParent* 
            
    '''
    def __init__(self):
        self.stick = 0
        
    def mousePressEvent(self, event):
        self.stick = 1
    
    def mouseReleaseEvent(self, event):
        self.stick = 0
        
    def mouseMoveEvent(self,event):
        
        mousePostion = event.globalPos()
        # independently shift my position by mouse position delta 
        if (self.stick and hasattr(self, "_lastMousePostion")):
            deltaX = mousePostion.x() - self._lastMousePostion.x();
            deltaY = mousePostion.y() - self._lastMousePostion.y();
            self.setGeometry(self.pos().x()+deltaX,self.pos().y()+deltaY,self.width(),self.height())
            
        self._lastMousePostion = mousePostion
    
                 
class ShapeButton(LogButton):
    
    def set_pen(self,pen):
        assert pen.__class__.__name__ == "QPen", "wrong pen class"
        self._pen = pen
        
    def get_pen(self):
        return self._pen
    
    pen = property(get_pen,set_pen)
    
    def __init__(self, 
                 parent=None, 
                 label="label", 
                 labelPosition = QtCore.QPoint(10,10),
                 foregroundColor = QtGui.QColor(0, 0, 0),
                 backgroundColor = QtGui.QColor(200, 200, 200),
                 show=True, 
                 size = QtCore.QSize(50,50),
                 styleSheet=bwStyleSheetBorderless):
        
        super(ShapeButton,self).__init__("", parent)
        self.setStyleSheet(styleSheet)
        self._pen = QtGui.QPen(foregroundColor)
        self._pen.setWidth(2)
        self._backgroundColor = backgroundColor
        self.resize(size)
        self.label = label
        self.labelPosition = labelPosition
        
        if not show:
            self.hide()
    
    def __str__(self):
        return self.__class__.__name__
    
    def getInnerRect(self):
        ''' get rectangle with respet to the drawing pen '''
        
        rect = QtCore.QRect(self.rect().x()+self._pen.width(),
                        self.rect().y()+self._pen.width(),
                        self.width()-self._pen.width()-1,
                        self.height()-self._pen.width()-1)
        return rect
    
class EllipseButton(ShapeButton,DeafWidget ):
    ''' class that is shown as circle '''       
    def __str__(self):
        return self.__class__.__name__
    
    def paintEvent(self, event):
        # init painter
        qpainter = QtGui.QPainter()
        qpainter.begin(self) 
        qpainter.setPen(self._pen)
        qpainter.setBrush(QtGui.QBrush(self._backgroundColor,QtCore.Qt.SolidPattern))
        
        # draw
        self._drawEllipse(event, qpainter)
        qpainter.drawText(self.labelPosition,self.label)    
        qpainter.end()
    
    def _drawEllipse(self, event, qpainter):
        rect = QtCore.QRect(self.rect().x()+self._pen.width(),
                        self.rect().y()+self._pen.width(),
                        self.width()-self._pen.width()-1,
                        self.height()-self._pen.width()-1)
        qpainter.drawEllipse(rect)
        
class PolygonButton(ShapeButton, DeafWidget):
    ''' class that is shown as convex polygon 
    
    draws a triangle by default
    
    '''
    
    def __str__(self):
        return str(len(self.get_points())) + self.__class__.__name__
     
    def get_points(self):
        return self._points
    
    def set_points(self, points):
        for point in points:
            assert point.x() <= 1.0 and point.x() >= 0, "point coordinates malformed"
            assert point.y() <= 1.0 and point.y() >= 0, "point coordinates malformed"
        self._points = points
    
    points = property(get_points,set_points) 
    
    def __init__(self, parent=None, 
                 styleSheet=bwStyleSheetBorderless, 
                 show = True,
                 label = "label",
                 labelPosition = QtCore.QPoint(10,10),
                 foregroundColor = QtGui.QColor(0, 0, 0),
                 backgroundColor = QtGui.QColor(200, 200, 200),
                 points=None,
                 size = QtCore.QSize(50,50)):
        
        super(PolygonButton,self).__init__(parent=parent, 
                                           label=label, 
                                           labelPosition = labelPosition,
                                           show=show, 
                                           size = size,
                                           foregroundColor = foregroundColor,
                                           backgroundColor = backgroundColor,
                                           styleSheet=styleSheet)

        # if no polygon is given, we will paint a triangle
        if points:
            self.points = points
        else:            
            self.points = [QtCore.QPointF(0., 1.), 
                          QtCore.QPointF(0.5, 0.), 
                          QtCore.QPointF(1., 1.),
                          QtCore.QPointF(0., 1.)]
    
    def paintEvent(self, event):
        # init painter
        qpainter = QtGui.QPainter()
        qpainter.begin(self)
        qpainter.setPen(self._pen)
        qpainter.setBrush(QtGui.QBrush(self._backgroundColor,QtCore.Qt.SolidPattern))

        # draw
        self._drawPolygon(event, qpainter)
        qpainter.drawText(self.labelPosition,self.label)
        qpainter.end()
    
    def _drawPolygon(self,event,qpainter):
        '''draw polygon from scaled points'''
        scaledPoints = []
        for point in self.points:
            scaledPoint = QtCore.QPointF(point)
            scaledPoint.setX(scaledPoint.x() * self.width())
            scaledPoint.setY(scaledPoint.y() * self.height())
            
            if scaledPoint.x() >= self.width()-self.pen.width():
                scaledPoint.setX(self.width()-self.pen.width())
            if scaledPoint.x() <= self.pen.width():
                scaledPoint.setX(self.pen.width())    
            if scaledPoint.y() >= self.height()-self.pen.width():
                scaledPoint.setY(self.height()-self.pen.width())
            if scaledPoint.y() <= self.pen.width():
                scaledPoint.setY(self.pen.width()) 
            scaledPoints.append(scaledPoint)
            
        polygon = Qt.QPolygonF(scaledPoints)
        qpainter.drawPolygon(polygon)

class RotationLabel(QtGui.QWidget):
    '''
    Rotating Label 
    '''
    def get_rotation(self):
        return self._rotation
    
    def set_rotation(self,value):
        self._rotation = value
        self.update()
    
    rotation = property(get_rotation,set_rotation)
       
    def __init__(self, 
                 label = "label", 
                 parent=None,
                 rotation = 45,
                 size = QtCore.QSize(150,150),
                 logHandler=sys.stdout, 
                 styleSheet=bwStyleSheetBorder):
        ''' initialize
        
        arguments:
            label - label string
            parent - parent widget
            rotation - rotation angle
            size - label size [QSize]
            logHandler - loging handler
            styleSheet
        '''
        
        super(RotationLabel,self).__init__(parent=parent)
        
        self.resize(size)
        self.label = label
        self.rotation = rotation
        self.update()
        
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setPen(QtCore.Qt.blue)
        painter.translate(20, 100)
        painter.rotate(self.rotation)
        painter.drawText(0, 0, self.label)
        painter.end()

class RotationButton(StickyButton,LogButton):
    
    def get_rotation(self):
        return self._rotation
    
    def set_rotation(self,value):
        self._rotation = value
        self.update()
    
    rotation = property(get_rotation,set_rotation)
    
    ''' Button with rotation content '''
    def __init__(self, pixmap, 
                 label = "label", 
                 parent=None,
                 rotation = 0,
                 size = QtCore.QSize(150,150),
                 logHandler=sys.stdout, 
                 styleSheet=bwStyleSheetBorder, 
                 styleSheetPressed=bwStyleSheetBorder,
                 show=True):
        
        StickyButton.__init__(self)
        LogButton.__init__(self,"",parent=parent,
                           logHandler=logHandler,
                           styleSheet=styleSheet,
                           styleSheetPressed=styleSheetPressed)
        
        self._scale = 1.0
        self.resize(size)
        self.pixmap = pixmap
        self.label = label
        self.rotation = rotation
        
        
    def paintEvent(self, event):
        qpainter = QtGui.QPainter(self)
        qpainter.setWindow(event.rect())
        # calculate translation vector to correct pic position after rotation
        # around the top left point
        center = np.array([event.rect().width()/2.,event.rect().height()/2.])
        rotAngle = math.radians(self.rotation)
        # rotate center of rectangle
        rotationMatrix = np.array([[math.cos(rotAngle),-1*math.sin(rotAngle)],
                                   [math.sin(rotAngle),math.cos(rotAngle)]])

        rotCenter = np.dot(center,rotationMatrix)
        # calculate difference to original and rotate back, since the new 
        # coordinate system is also rotated
        translation = (center - rotCenter)
        rotationMatrix = np.array([[math.cos(-rotAngle),-1*math.sin(-rotAngle)],
                                   [math.sin(-rotAngle),math.cos(-rotAngle)]])

        translationNewCoordinateSystem = np.dot(translation,rotationMatrix)
        print translation
        # move rotated picture to old center point
        #print tx,ty
        qpainter.rotate(self.rotation)
        qpainter.translate(QtCore.QPointF(translationNewCoordinateSystem[1],
                                          translationNewCoordinateSystem[0]))
        
        qpainter.drawPixmap(event.rect(), self.pixmap)
        qpainter.drawText(event.rect().center(), self.label)

class PositionRotationButton(RotationButton):
    
    ''' automatic alignment '''
    def __init__(self, pixmap, 
                 label = "label",
                 borderWidth = 100, 
                 parent=None,
                 rotation = 0,
                 size = QtCore.QSize(150,150),
                 logHandler=sys.stdout, 
                 styleSheet=bwStyleSheetBorder, 
                 styleSheetPressed=bwStyleSheetBorder,
                 show=True):
        
        
        RotationButton.__init__(self, pixmap, 
                 label = label, 
                 parent=parent,
                 rotation = rotation,
                 size = size,
                 logHandler=logHandler, 
                 styleSheet=styleSheet, 
                 styleSheetPressed=styleSheetPressed,
                 show=show)
    
    def mouseMoveEvent(self,event):
        x =  self.globalPos().x()
        y = self.globalPos().y()
        print x,y
        delta = 100
        if  x < delta and y < delta :
            self.rotation = 135
        elif x > self.parent.width() - delta and y < delta:
            self.rotation = -135
        elif x > self.parent.width() and y > self.parent.height()-delta:
            self.rotation = -45
        elif x < delta and y > self.parent.height()-delta:
            self.rotation = 45
        elif x < delta:
            self.rotation = 90
        elif x > self.parent.width()-delta:
            self.rotation = -90
        elif y < delta:
            self.rotation = 180
        elif y > self.parent.height()-delta:
            self.rotation = 0  
        else:
            self.rotation = 0
        
        super(RotationButton,self).mouseMoveEvent(event)
        
class PicLabelButton(LogButton, DeafWidget):
    
    
    ''' Button containing a pixmap and a label '''
    def __init__(self, pixmap, 
                 label = "label", 
                 parent=None,
                 rotation = 0,
                 size = QtCore.QSize(100,100),
                 fragmentationFactor = 1./2.,
                 logHandler=sys.stdout, 
                 styleSheet=bwStyleSheetBorder, 
                 styleSheetPressed=bwStyleSheetBorder,
                 show=True):
        super(PicLabelButton, self).__init__("",parent=parent,
                                             logHandler=logHandler,
                                             styleSheet=styleSheet,
                                             styleSheetPressed=styleSheetPressed)
        
        self.resize(size)
        self.pixmap = pixmap
        self.fragmentationFactor = fragmentationFactor
        self.label = label
        self.rotation = rotation
        

    def paintEvent(self, event):
        qpainter = QtGui.QPainter(self)        
        rect = QtCore.QRect(event.rect())
        
        # centered upper fragmentationFactor rectangular
        rect.setHeight(event.rect().height() * self.fragmentationFactor)
        rect.setWidth(event.rect().width() * self.fragmentationFactor)
        rect.moveTo((1-self.fragmentationFactor)/2,0 )
        
        qpainter.drawPixmap(rect, self.pixmap)
        
        # centered lower rest rectangular (1-...)
        rect.setHeight(event.rect().height() * (1.-self.fragmentationFactor))
        rect.moveTo((1-self.fragmentationFactor)/2,event.rect().height() * self.fragmentationFactor)
        rect.setWidth(event.rect().width() * self.fragmentationFactor)
        qpainter.drawText(rect,QtCore.Qt.AlignHCenter,self.label)
        
        
    def sizeHint(self):
        return self.pixmap.size()

class LabelPicButton(PicLabelButton):
    ''' Button containing a pixmap and a label '''
    
    def paintEvent(self, event):
        
        qpainter = QtGui.QPainter(self)
        rect = QtCore.QRect(event.rect())
        
        rect.setHeight(event.rect().height() * (1.-self.fragmentationFactor))
        rect.setWidth(event.rect().width())
        rect.moveTo(0,0 )
        qpainter.drawText(rect,QtCore.Qt.AlignHCenter,self.label)
                
        rect.setHeight(event.rect().height() * self.fragmentationFactor)
        rect.setWidth(event.rect().width() * self.fragmentationFactor)
        rect.moveTo(event.rect().width() * (1-self.fragmentationFactor)/2.,
                    event.rect().height() * (1.-self.fragmentationFactor))
        qpainter.drawPixmap(rect, self.pixmap)


#deprecated 
class DeafQCheckBox(QtGui.QCheckBox,LogButton, DeafWidget):

    def __init__(self, *args,**kwargs):
        super(DeafQCheckBox,self).__init__(*args,**kwargs)

#deprecated 
class DeafQRadioButton(QtGui.QRadioButton,LogButton, DeafWidget):

    def __init__(self, title, parent=None, show=True):
        super(QtGui.QRadioButton,self).__init__(title, parent)
        self.parent = parent
        self.setStyleSheet(bwStyleSheetBorderless)
        
        if not show:
            self.hide() 

