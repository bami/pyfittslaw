#!/usr/bin/python
'''
XRandomClicker.py
    Copyright 2010 by Bastian Migge <miggeb@ethz.ch>

logs
    * click position relative to the viewpoint (at the center of the windows
    * mouse click position relative to center of the target button

output
    global _x and _y position of the click and the relative error to the target (button)
       |
      -|-----> _x
       |
       |
     _y v
  
    (0,0) at upper left

function
    targets moves randomly after been hit

'''

#@todo: rewrite this mess
import sys
sys.path.append('../../CorrectionController/src/')
sys.path.append('../../_tracking/qualisysLocator/src/')

import pickle
import time
import random
import logging
from PyQt4 import QtGui, QtCore
from optparse import OptionParser
import numpy as np
from ClickTest import *
from GUI.Buttons import LogButton

LOG_FILENAME = 'XRandomClicker.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)

# global og file handler
headPositionLogFileHandler = sys.stdout
clickLogFileHandler = sys.stdout

# global constant
epsilon = 1e-6

buttonHeight = 15
buttonWidth = 15

targetClickTransitionMatrix = []
# bucket count in _x and _y
bucketCount = []
        
class MovingButtonWidget_TransitionPDF(AbstractClickTestWidget):   
    ''' A 2 dimensional density function defines the transition prbability of targets
    elatively on an equidistant discretisation
    '''    
    # density function that describes the transition probability
    # from a given click position (the current one) to the next one 
    # function is defined linear over bucketCount[0] _x bucketCount[1]
    # each bucket represents a certain screen section
    # index is defined row-wise:
    # 1 2 3
    # 4 5 6
    densityFunction = []
    # bucket count in _x and _y
    bucketCount = []
    
    bucketSize = np.array([])
    
    logHandler = None
    
    def get_activeButtonIndex(self):
        pass
    
    def set_activeButtonIndex(self,buttonIndex):
        pass
    
    activeButtonIndex = property(get_activeButtonIndex,set_activeButtonIndex)
    
    def get_activeButton(self):
        return self.button
    
    activeButton = property(get_activeButton)
    
    def __init__(self, button=None, parent=None,logHandler=sys.stdout):
        
        self.logHandler = logHandler
        
        self.densityFunction = targetClickTransitionMatrix
        # bucket count in _x and _y
        self.bucketCount = bucketCount
        self.parent = parent
        
        self._sanityCheck()
        QtGui.QWidget.__init__(self, parent)
        
        # close app signal connect (see keyPressEvent())
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'), QtCore.SLOT('close()') )
        
        self.setWindowTitle("PDF Single Clicker")
        
        # init button
        if (button):
            self.button = button
        else:
            self.button = LogButton('+', self, logHandler=None)
        
        #self.button.setGeometry(random.random() * self.width()-buttonWidth, random.random() * self.height()-buttonHeight, buttonWidth, buttonHeight)
        # disable cursor feedback
        #self.setCursor(QtCore.Qt.BlankCursor)
        
        # widget setup
        self.setWindowTitle(str(self.__class__))
        #self.setCursor(QtCore.Qt.BlankCursor) # disable cursor feedback
        #self.showMaximized()
        self.setWindowState(QtCore.Qt.WindowFullScreen)
        
        return
    
    def resizeEvent(self, QResizeEvent):
        '''
        adapt bucket size of relative probability density
        '''
        self.bucketSize = np.array([self.width() / self.bucketCount[0], self.height() / self.bucketCount[1]])
        return  

    def mousePressEvent(self, event):
        '''
        on click handler if button passed event
        '''

        message = {
                   "global click position:" : [event.globalPos().x(),event.globalPos().y()],
                   "native pointing error: " : [event.x() - self.button.geometry().center().x(), event.y() - self.button.geometry().center().y()],
                   "target: " : [self.button.geometry().center().x(), self.button.geometry().center().y()],
                   "target size:" : [self.button.geometry().width(),self.button.geometry().height()],
                   }
        
        print >>self.logHandler , message 
        
    def updateTarget(self):
        newPosition = self.__getNextButtonPosition(self.button.pos().x(),self.button.pos().y())
        self.button.setGeometry(newPosition.x(),newPosition.y(),self.button.width(),self.button.height())
        return
    
    def __getNextButtonPosition(self, currentPosX, currentPosY):
        #@deprecated: inplemented in button 
        ''' generate next position randomly 
        based on the current position and the probability density
        '''
        
        currentPositionIndex = self._getDensityBucketIndexOfCurrentPosition(currentPosX, currentPosY)
        # generate distribution function
        distributionFunction = []
        
        # get next target index by sampling the density Function
        # by sampling uniform over the distribution function
        nextTargetIndex = np.array([])
        
        densityFunctionForCurrentPosition = self.densityFunction[currentPositionIndex]
        acc = 0.0
        for bucketIndex in range(len(densityFunctionForCurrentPosition)):
            acc += densityFunctionForCurrentPosition[bucketIndex]
            distributionFunction.append(acc)

        r = random.random()
        for bucketIndex in range(len(distributionFunction)):
            if (r <= distributionFunction[bucketIndex]):
                # convert linear index to 2d index
                nextTargetIndex = np.array([int(bucketIndex % self.bucketCount[1]), int(bucketIndex / self.bucketCount[1])])
                #print currentPositionIndex, "(", [currentPosX, currentPosY] ,")","->", bucketIndex, "(", nextTargetIndex, ")"
                break

        # convert nextTargetIndex to a display position
        nextTargetPosition = nextTargetIndex * self.bucketSize
        # sample uniform within the bucket (screen section)
        nextTargetPosition[0] += random.random() * self.bucketSize[0] % (self.bucketSize[0] - buttonWidth)
        nextTargetPosition[1] += random.random() * self.bucketSize[1] % (self.bucketSize[1] - buttonHeight)
        
        #return QtCore.QPoint(nextTargetPosition[0], nextTargetPosition[1])
        return QtCore.QPoint(1,1)
    
    def _getDensityBucketIndexOfCurrentPosition(self, xPos, yPos):
        #@deprecated: inplemented in button 
        index = [int(yPos) / int(self.bucketSize[1]), int(xPos) / int(self.bucketSize[0])]
        
        linearIndex = index[0] * self.bucketCount[1] + index[1]
        assert linearIndex < len(self.densityFunction), "index out of bounce: " + str(index)
        #print index,"->", linearIndex
        return linearIndex
    
    def _sanityCheck(self):
        
        # check if the density function corresponds to the bucketCount
        for row in self.densityFunction:
            assert len(row) == len(self.densityFunction), "error in density function row: length is " + str(len(row)) + "," + str(self.densityFunction)
        
        # check if the given dimensions in bucketCount match the transition matrix
        dimensionCount = 1.0
        for bin in self.bucketCount:
            dimensionCount *= bin
        
        assert dimensionCount == len(self.densityFunction), "error in density function: " + str(self.densityFunction)
        
        for bucket in self.densityFunction:
            assert (sum(bucket) - 1.0 < epsilon), "density function corrupt: " + str(bucket)
            assert len(bucket) == len(self.densityFunction), "density function corrupt: " + str(bucket)        
        return     
        
if __name__ == "__main__":
    
    headTracker = None
    widgetArguments = {}
    trackerArguments = {}
    bucketCount = []
            
    # parse arguments
    parser = OptionParser()
    
    parser.add_option("-T", "--targetClickTransitionMatrixFile",
                      action="store", 
                      help="target click transition matrix (pickle format)", metavar="FILE")
    
    parser.add_option("-r", "--rowCount",
                      action="store",
                      help="number of rows in target click transition matrix",metavar="INT")
    
    parser.add_option("-c", "--colCount",
                      action="store",
                      help="number of columns in target click transition matrix",metavar="INT")
    
    parser.add_option("-C", "--clickLogFileName", 
                  help="write click to log file", 
                  default=None, metavar="FILE")
    
    parser.add_option("-v", "--verbose",
                      action="store", dest="verbose", default=False,
                      help="print status messages to stdout")
    
    
    (options, args) = parser.parse_args()
        
    if(options.clickLogFileName):
        logging.info("logging clicks  to file: " + str(options.clickLogFileName))
        clickLogFileHandler = open(options.clickLogFileName,'w')
        clickLogFileHandler.write("#xrandomclicker log file\nvpX vpY vpZ clickX clickY clickZ targetX targetY targetZ unixtime\n")
        clickLogFileHandler.flush()
        widgetArguments['logHandler'] = clickLogFileHandler
    
    # evaluate parameter
    if (options.targetClickTransitionMatrixFile):
        logging.info('loading transition matrix from file') 
        transitionMatrixDumpFile = open(options.targetClickTransitionMatrixFile,'rb')
        targetClickTransitionMatrix = pickle.load(transitionMatrixDumpFile)
        transitionMatrixDumpFile.close()
                
        if (not options.rowCount or not options.colCount):
            logging.error('number of rows and columns not given!\n')
            sys.stderr.write('Error. See logfile for details. Exiting.\n')
            sys.exit()
        bucketCount = [int(options.colCount),int(options.rowCount)]
            
    else:
        logging.info('loading default transition matrix\n')
        targetClickTransitionMatrix = [[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0], [1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0]]
        bucketCount = [2,2]
    
    startTime = time.time()
        
    app = QtGui.QApplication(sys.argv)

    mw = MovingButtonWidget_TransitionPDF(**widgetArguments)
    
    #######
    #samplingPoints = [[320, 187], [320, 561], [320, 935], [960, 187], [960, 561], [960, 935], [1600, 187], [1600, 561], [1600, 935]] 
    #samplingPointErrors = [[2, 1], [1, 2], [1, 2], [1, -1], [1, 2], [1, 2], [1, 1], [2, 0], [-10, -9]] 
    #correctionController = PointingErrorCorrectionController_FangChang(samplingPoints, samplingPointErrors)
    #mw = RandomButtonWidgetCorrected(correctionControllers=[correctionController])
    #######
    
    mw.show()    
    app.exec_()
    
    # log duration
    endTime = time.time()
    print >> clickLogFileHandler, "#duration in seconds: " + str(endTime-startTime)
            
    # close logger
    clickLogFileHandler.flush()
    headPositionLogFileHandler.flush()
    
    clickLogFileHandler.close()
    headPositionLogFileHandler.close()
    
    app.closeAllWindows() 
    print "the end"
    sys.exit()

