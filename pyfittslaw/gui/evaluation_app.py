# -*- coding: utf-8 -*-
"""
    pyfittslaw.gui.evalutaion_app
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: Pointing Error Evaluation Application
"""

from PyQt4 import QtCore
from pyqtpc.gui.pyqt4.app.x11 import QApplicationPointingErrorCorrectedX11Simulation, QApplicationPointingErrorCorrectedX11
from pyqtpc.gui.x11.x11Event import PyXevent
from pyqtpc.evaluation import EvaluationApplication
from pyqtpc.correction.control.static import StaticCorrectionController

class QApplicationMultiControllerEvaluation(QApplicationPointingErrorCorrectedX11Simulation,EvaluationApplication):
    """
    Python Qt X11 Application that evaluated the correction performance of multiple controllers


    The application supports evaluation from pyqtpc.evaluation.EvaluationApplication
    and record and replay from QApplicationPointingErrorCorrectedSimulation.

    Only 'MouseReleaseEvent' are considered !
    """

    def __init__(self,*args,**kwargs):
        """
        Initialize

        arguments:
            evaluated_controllers - list of controllers to be evaluated
            correction_controller_index - index of applied correction controller (default: no correction)
            dump_filename - filename of logfile
        """
        EvaluationApplication.__init__(self,dump_filename = kwargs['dump_filename'])
        del kwargs['dump_filename']

        no_correction = StaticCorrectionController(correction_offset=[0,0])
        no_correction.name = 'NO_CORRECTION'
        self.errorKey = no_correction.name
        self.add_controller(no_correction)

        if kwargs.has_key('correction_controller_index'):
            kwargs['correction_controller'] = kwargs['evaluated_controllers'][kwargs['correction_controller_index']]
            del kwargs['correction_controller_index']
        else:
            kwargs['correction_controller'] = no_correction

        for c in kwargs['evaluated_controllers']:
            self.add_controller(c)
        del kwargs['evaluated_controllers']

        super(QApplicationMultiControllerEvaluation,self).__init__(*args,**kwargs)

        self.connect(self, QtCore.SIGNAL('aboutToQuit()'), self.show_log)

    def x11EventFilter(self, event):
        """
        evaluate controllers, log stuff, forward call to parent, who will eventually apply a correction

        Annotation
            Since QApplication.x11ProcessEvent can not be overwritten in PyQt,
             we implement the event management here.

        Arguments
            event - X11 event (pointer to C struct see Xlib manual)
        """

        # extract native x11Event event in C to python dict
        # http://docs.python.org/release/2.5.2/api/cObjects.html
        cObject = event.ascobject()
        pyEvent = PyXevent.getEventAsPyDict(cObject)

        # filter event
        if not self.validEvent(pyEvent):
            return False

        # record interaction
        QApplicationPointingErrorCorrectedX11Simulation.logInteractionEvent(self,pyEvent)

        # evaluate controllers
        self.evaluate_controllers(pyEvent)

        return QApplicationPointingErrorCorrectedX11.x11EventFilter(self,event)

