# -*- coding: utf-8 -*-
"""
    pyfittslaw.utils.targetClickTransitionMatrixGenerator
    ~~~~~~

    :copyright: (c) 2010 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description:
    Generator for transition matrix of the tests.XRandomClicker
"""

import sys
import os
import pickle
import numpy as np

fileName = "clickTransition.dat"
bucketCount = [30,30]

''' generate transition independent 1d triangle distribution '''

def triangleIncrease(bucketCount):
    h = 2./bucketCount
    buckets = np.array(range(1,bucketCount+1),"float") # fill
    buckets /= np.sum(buckets) # normalize
    return buckets

def triangleDecrease(bucketCount):
    a = triangleIncrease(bucketCount)
    return a[::-1]

''' generate V distribution '''
def VDistribution(bucketCount):
    bucketsA, bucketsB = triangleIncrease(bucketCount/2), triangleDecrease(bucketCount/2) 
    
    # merge
    v = bucketsA.tolist()
    v.extend(bucketsB.tolist())
    v = np.array(v)
    
    v /= np.sum(v) # normalize
    return v

def invertDistribution(distributionArray):
    acc = np.sum(distributionArray)
    
    a = distributionArray.tolist()
    inv = map(lambda x: acc-x, a)
    inv = np.array(inv)
    inv /= np.sum(inv) # normalize
    return inv

''' generate 2dimensional distribution '''
def mix2D(buckets1, buckets2):
    matrix = []
    
    for bucketIndex1 in range(bucketCount[0]):
        row = buckets1[bucketIndex1] * buckets2
        matrix.append(row)
    matrix = np.array(matrix)
    matrix /= np.sum(matrix)
    return matrix

def flatenMatrix(matrix2D):
    result = []
    list = matrix2D.tolist()
    for row in list:
        result.extend(row)
    return np.array(result)

print "calculating matrixes ..."

buckets1, buckets2 = triangleIncrease(bucketCount[0]),triangleIncrease(bucketCount[1])
matrixTriangularIncrease = mix2D(buckets1,buckets2)

buckets1, buckets2 = triangleDecrease(bucketCount[0]),triangleDecrease(bucketCount[1])
matrixTrianglularDecrease = mix2D(buckets1,buckets2)

bucketsMale = VDistribution(bucketCount[0])
bucketsFemale = invertDistribution(bucketsMale)

matrix1 = mix2D(bucketsMale,bucketsFemale)
matrix2 = mix2D(bucketsFemale,bucketsMale)

#print matrixTriangularIncrease
#print matrixTrianglularDecrease
#print matrix1, matrix2

buckets = reduce(lambda x,y: x*y, bucketCount)
transitionMatrix = []

for bucketIndex in range(buckets):
    transitionMatrix.append(flatenMatrix(matrix2).tolist())

#print transitionMatrix

print "writing to file ... "
# write to file
transitionMatrixDumpFile = open(fileName,'w')
pickle.dump(transitionMatrix, transitionMatrixDumpFile)
transitionMatrixDumpFile.close()

print "done."