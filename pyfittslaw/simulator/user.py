# -*- coding: utf-8 -*-
"""
    ViewpointSimluator
    ~~~~~~

    :copyright: (c) 2011 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: simulation of the users viewpoint dynamics 
"""

from numpy import *
from pyqtpc.utils.ProbabilityDistribution import ProbabilityDistribution

class ViewpointSimulator(object):
    def __init__(self, 
                 userMovement, 
                 userPosition, 
                 observations, 
                 dexterityModel, 
                 screenOffset):
        """
        init method
        
        arguments:
            userMovement
            userPosition
            observations
            dexterityModel
            screenOffset
        """
        self.__userMovement = userMovement
        self.__userPosition = userPosition
        self.__observations = observations
        self.__dexterityModel = dexterityModel
        self.__screenOffset = screenOffset
    
    def stateSpace(self):
        return self.__userPosition.scale.tolist()
    
    def observationSpace(self):
        return self.__observations
    
    def getUserPositionBucketIndex(self, position):
        ''' return the bucket index of the given position  
        (proxy to user model) '''
        return self.__userPosition.getBucketIndex(position)
    
    def getObservationBucketIndex(self, observation):
        ''' return observation index '''
        observationId = 0
        observations = self.__observations
        for oIndex in range(len(observations)-1):
            observationId = oIndex
            if observations[oIndex] > observation:
                break
        return observationId
    
    def userPositionNext(self, currentPosition):
        ''' return the 1D next user position give the current position'''
        # combine movement distribution and global user position
        sampleDistribution = self.__userMovement.Shift(currentPosition) * self.__userPosition

        # sample new position and set scale
        nextPosition = sampleDistribution.sample()
        return nextPosition
            
    def interactionError(self, viewpoint):
        """ samples the 1D click error 
        
        Keyword arguments:
        viewpoint - the normalized viewpoint relative to the target  
        offset - the offset between interaction and display plane
        
         target ---B
            \      |
           touch --A
              \    |
               \   |
                \  |
                 \ |
                  VP
         
         VP / 1 = (touchA) / (AB)
         AB = offset
         VP * offset = touchA
         error = targetB - touchA
         targetB = VP
         errorMean = VP - (VP * offset) 
        """
        errorMean = -1 * (viewpoint - (viewpoint * self.__screenOffset))
        error = errorMean + self.__dexterityModel.sample()
        return error
    
    def simulate(self, timeSteps, initialPosition = 0):
        # absolute values
        positions = [0] * timeSteps
        interactionErrors = [0] * timeSteps
        # state and observation ids
        positionBuckets = [0] * timeSteps 
        observationBuckets = [0] * timeSteps
        
        positions[0] = initialPosition
        
        for step in range(timeSteps-1):
            pos = positions[step]
            
            interactionErrors[step] = self.interactionError(pos)
            positions[step+1] = self.userPositionNext(pos)
            
            observationBuckets[step] = self.getObservationBucketIndex(interactionErrors[step])
            positionBuckets[step+1] = self.getUserPositionBucketIndex(positions[step+1])
            
        
        return {"positions" : positions, 
                "states" : positionBuckets, 
                "interactionErrors" : interactionErrors,
                "observations" : observationBuckets,
                "timeSteps" : timeSteps }

if __name__ == '__main__':
    
    # user behavior 
    userMovement = ProbabilityDistribution([1./4.] * 4,range(-2,3)) # relative movement from 0 
    userPosition = ProbabilityDistribution([1./10.] * 10,range(-5,6)) # overall position
    dexterityModel = ProbabilityDistribution([0.1,0.4,0.4,0.1],range(-2,3))
    observations = range(-2,3)
    # sample user position and resulting click error
    user = ViewpointSimulator(userMovement, userPosition, observations, dexterityModel, 3)
    print user.simulate(10)