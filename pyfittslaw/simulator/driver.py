# -*- coding: utf-8 -*-
"""
    pyfittslaw.simulator.simulator
    ~~~~~~

    :copyright: (c) 2011,2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: controls pointing device driver
    PointerControllerXdo needs xdotools (binaries)
    PointerControllerXlib needs python xlib bindings
"""
import os
from ctypes import cdll
from Xlib import *
import math
import pyqtpc.correction.control as controllercore
from collections import namedtuple

class PointerDriverInterface(object):
    """
    Pointer simulator

    controls the pointer of the graphical user interface

    """
    def move(self,pos):
        """
        move pointer to target position

        arguments:
            pos - tuple of absolute display position
        """
        raise NotImplementedError()

    def press(self,pos):
        """
        press pointer at current position
        """
        raise NotImplementedError()

    def release(self,pos):
        """
        release pointer at current position
        """
        raise NotImplementedError()

    def click(self,pos):
        """
        click pointer at current position
        """
        raise NotImplementedError()

    def key(self, key):
        """
        press key
        """
        raise NotImplementedError()

class XdoDriver(PointerDriverInterface):
    """
    pointer controller using the XDO tool shell interface
    """

    moveCommand = "xdotool mousemove"
    downCommand = "xdotool mousedown 1"
    upCommand = "xdotool mouseup 1"
    clickCommand = "xdotool click 1"
    keyCommand = "xdotool key {key}".format

    def move(self,pos):
        os.system(XdoDriver.moveCommand + " " + str(pos[0]) + " " + str(pos[1]))

    def press(self,pos):
        self.move(pos)
        os.system(XdoDriver.downCommand)

    def release(self,pos):
        self.move(pos)
        os.system(XdoDriver.upCommand)

    def click(self, pos):
        self.move(pos)
        os.system(XdoDriver.clickCommand)

    def key(self, key):
        os.system(XdoDriver.keyCommand(key=key))

class X11Driver(PointerDriverInterface):
    """
    pointer controller using the x11 interface
    """

    def __init__(self, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self._x11= cdll.LoadLibrary('libX11.so')
        self._display    = self._x11.XOpenDisplay(0)
        self._rootwindow = self._x11.XDefaultRootWindow(self._display)

        super(X11Driver,self).__init__(*args, **kwargs)

    def __del__(self):
        self._x11.XCloseDisplay(self._display)
        super(X11Driver,self).__del__()

    def move(self,pos):
        self._x11.XWarpPointer(self._display,0,self._rootwindow,0,0,0,0,pos[0],pos[1])

    def click(self, pos):
        """
        click pointer at current position
        """
        self.press(pos)
        self.release(pos)


CorrectionControllerIOStimulatorTarget = namedtuple("CorrectionControllerIOStimulatorTarget",
    ['position','size'])

class ControllerDriver(object):
    """
    CorrectionControllerIO Stimulator

    directly stimulated the correction controller
    """

    def __init__(self, controllers, targets):
        """
        init direct pointer simulator

        arguments:
            controllers -- list of simulated controllers
                [pyqtpc.correction.controller.CorrectionControllerIO]
            targets -- list of [CorrectionControllerIOStimulatorTarget]
        """

        assert len(targets), "no target given!"

        self.controllers = controllers
        self.targets = targets

        super(ControllerDriver,self).__init__()

    def _distance(self,position_a, position_b):

        offset = [t-i for t,i in zip(position_b, position_a)]
        distance = math.sqrt(reduce(lambda x,y: x + y**2, offset))
        return distance

    def _closest_target(self, interaction_position):
        closest_target = self.targets[0]
        error = self._distance(interaction_position,closest_target)

        for target in self.targets:
            candidate_center = self._point_of_attention(target)
            candidate_error = self._distance(interaction_position, candidate_center)
            if candidate_error < error:
                error = candidate_error
                closest_target = target

        return closest_target

    def _point_of_attention(self,target):
        """
        return the point of attention of an area target
        """
        [p + (s/2.) for p,s in target.sizezip(target.position, target.size)]

    def release(self, pos):
        """
        release pointer at current position
        """

        # generate sense data
        target = self._closest_target(pos)
        target_center = self._point_of_attention(target)

        sense_data = controllercore.CorrectionControllerIOSenseData()
        sense_data.pointing_error=self._distance(pos,target_center)
        sense_data.target_size=target.size
        sense_data.pointing_position = pos
        print sense_data
        # pass sense data to each controller
        for c in self.controllers:
            c.sendSensorData(self.sense_data)
