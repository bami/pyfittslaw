# -*- coding: utf-8 -*-
"""
    pyfittslaw.simlation
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :decrption: interaction simulation package
"""

import time
import re
from threading import Thread

class PointerEventRecorder(object):
    def __init__(self,filename=None, **kwargs):

        self.filename = filename

        super(PointerEventRecorder,self).__init__(**kwargs)
        self.logData = []

    def addLogEntry(self,timeStamp, typ, globalX, globalY ):
        """
        add log entry

        arguments:
            timestamp
            type
            globalX
            globalY
        """

        dataEntry = [timeStamp, typ, globalX, globalY]
        try:
            PointerEventPlayer.checkLogEntry(dataEntry)
            self.logData.append(dataEntry)
        except Exception:
            print "ignored: ", dataEntry
            pass

    def writeToFile(self,filename=None):
        """
        write log data to file

        arguments:
            filename - name of the log file
        """
        fn = filename if filename else self.filename

        with open(fn, 'w') as f:
            for entry in self.logData:
                strEntry = [str(x) for x in entry]
                f.write(",".join(strEntry))
                f.write("\n")

        print "stored %i events" % PointerEventPlayer.numberOfEvent(self.logData)

class PointerEventPlayer(Thread):
    """
    Pointer Event Player
    (http://www.semicomplete.com/projects/xdotool/)

    log file format:
    CSV
    [timestamp in s,type in [MouseMoveEvent,MousePressEvent,MouseReleaseEvent], ...
    globalPositionX as int, globalPositionY as int]

    """
    header = ['timestamp','type','globalPositionX','globalPositionY']
    Types = ['MousePressEvent','MouseReleaseEvent','MouseMoveEvent']

    ignoreLine = re.compile("^[\s+#'ERROR']")

    @staticmethod
    def initFromLogfile(driver,
                        filename,
                        realTime = True,
                        epochLag = 0.,
                        initLag = 0.):
        """
        init simulator from a log file

        arguments:
            driver - the simulator interface [pyfittslaw.simulation.driver.PointerDriverInterface]
            filename - the file name of the log file
            realTime - real time switch
            epochLag - epoch Lag between interactions, used in real time mode [s]
            initLag - inital delay [s]
        """
        f = open(filename, 'r')
        logData = []
        for line in f.readlines():
            # ignore empty lines
            if not len(line):
                continue
            if (PointerEventPlayer.ignoreLine.match(line)):  # ignore line
                continue

            line = line.rstrip()
            data = line.rsplit(",")

            try:
                dataFormatted = [float(data[0]),
                                 str(data[1]),
                                 int(data[2]),
                                 int(data[3])]

                PointerEventPlayer.checkLogEntry(dataFormatted)
            except:
                print("invalid line in log file: " + line)
                raise

            logData.append(dataFormatted)

        instance = PointerEventPlayer(simulator = driver,
            logData=logData,
            realTime= realTime,
            epochLag = epochLag,
            initLag = initLag)

        return instance

    @staticmethod
    def checkLogEntry(logEntry):
        """
        check log entry

        arguments:
            logEntry
        """
        if len(logEntry) > len(PointerEventPlayer.header):
            raise Exception("invalid length")

        if not logEntry[1] in PointerEventPlayer.Types:
            raise Exception("Invalid type")

        if logEntry[0] < 0:
            raise Exception("Invalid time")

        if logEntry[2] < 0 or logEntry[3] < 0:
            raise Exception("Invalid pointer position")

    @staticmethod
    def numberOfEvent(logData, eventName = None):
        """
        return the number of events

        arguments:
            eventName - name of event (e.g. 'MousePressEvent')
        """
        count = 0

        if eventName is None:
            return len(logData)

        for index in range(logData):
            if logData[index][1] == eventName:
                count += 1

        return count

    @staticmethod
    def _unpackLogEntry(dataEntry):
        """
        transform log dataEntry into dict
        """

        d = {}
        for index in range(len(dataEntry)):
            h = PointerEventPlayer.header[index]
            d[h] = dataEntry[index]

        return d

    @staticmethod
    def reduce_log(logData):
        """
        remove cursor moves pairs and tailing moves
        """
        logDataCleaned = []

        #remove heading mouseMoves
        for index in range(0,len(logData)):
            d = PointerEventPlayer._unpackLogEntry(logData[index])
            if not (d['type'] == 'MouseMoveEvent'):
                logDataCleaned = logData[index:]
                break

        logData = logDataCleaned
        logDataCleaned = []

        #remove tailing mouseMoves
        for index in range(len(logData)-1,-1,-1):
            d = PointerEventPlayer._unpackLogEntry(logData[index])
            if not (d['type'] == 'MouseMoveEvent'):
                logDataCleaned = logData[0:index+1]
                break

        logData = logDataCleaned
        logDataCleaned = []
        dlast = {'type': None}

        # remove pairs of MouseMoves
        for logEntry in logData:
            d = PointerEventPlayer._unpackLogEntry(logEntry)
            if not (dlast['type'] == 'MouseMoveEvent' and
                    d['type']     == 'MouseMoveEvent'):
                logDataCleaned.append(logEntry)
            dlast = d

        return logDataCleaned

    def __init__(self,
                 simulator,
                 logData,
                 realTime = True,
                 epochLag = 0.,
                 initLag = 0.,
                 init_key = None,
                 final_key = "Escape"):
        """
        initialize and start x11 event simulator

        arguments:
            simulator - [PointerSimulatorInterface]
            logData - list of log data (format defined in PointerEventPlayer.header)
            realtime - run simulation in real time mode [True|False]
            epochLag - waiting time between two events if not in real time mode [s]
            initLag - initial waiting time [s]
            init_key - key stroke sent before starting the simulation (default:None)
            final_key - key stroke sent after the simulation (default:Escape) [xdotool format]
        """
        Thread.__init__(self)

        assert len(logData) > 0, "No log data given."

        self.driver = simulator
        self.realTime = realTime
        self.epochLag = epochLag
        self.initLag = initLag # initial wait
        self.logData = PointerEventPlayer.reduce_log(logData)
        self.init_key = init_key
        self.final_key = final_key

        assert len(self.logData) > 0, "No valuable log data given."

        data = PointerEventPlayer._unpackLogEntry(self.logData[0])

        self.startTime = data['timestamp']

        print "initialized with %i events" % PointerEventPlayer.numberOfEvent(self.logData)

        self.start()

    def run(self):
        print "\a" # beep
        # waiting for init lag
        time.sleep(self.initLag)

        # send initial key stroke
        if self.init_key:
            self.driver.key(self.init_key)

        lastTime = self.startTime
        for dataEntry in self.logData:
            if self.realTime:
                epochLength = dataEntry[0] - lastTime
                lastTime = dataEntry[0]
                time.sleep(epochLength)
            else:
                time.sleep(self.epochLag)

            self.controlCursor(dataEntry)

        #  send final key stroke

        if self.final_key:
            self.driver.key(self.final_key)
        print "\a" # beep

    def controlCursor(self,logEntry):
        """
        control cursor from logEntry

        change status (down,up) and then move cursor

        arguments:
            logEntry - log entry
        """

        d = PointerEventPlayer._unpackLogEntry(logEntry)
        pos = [d['globalPositionX'],d['globalPositionY']]

        self.driver.move(pos)

        if d['type'] == "MousePressEvent":
            self.driver.press(pos)

        elif d['type'] == "MouseReleaseEvent":
            self.driver.release(pos)

if __name__ == "__main__":
    logger = PointerEventRecorder()

    logger.addLogEntry(0, "MouseMoveEvent", 444, 472)
    logger.addLogEntry(1, "MousePressEvent", 444, 472)
    logger.addLogEntry(2, "MouseMoveEvent", 500, 500)
    logger.addLogEntry(3, "MouseReleaseEvent", 500, 500)
    logger.writeToFile("testLog.log")

    simulator = core.PointerSimulatorXdo()
    sim = PointerEventPlayer.initFromLogfile(simulator, "testLog.log")
    sim.realtime = False


